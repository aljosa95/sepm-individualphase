# SEPM Einzelphase

## Author

Name: Abadzic, Alija-Ali\
Matrikelnummer: e01427575

## Kurzanleitung

Das Programm kann mit dem Befehl:\
```mvnw compile```\
gebaut werden können.

Das Programm kann mit dem Befehl:\
```mvnw test```\
getestet werden können.

Das Programm kann mit dem Befehl:\
```mvnw exec:java```\
ausgeführt werden.# SEPM - 01427575
