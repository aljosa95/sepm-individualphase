package at.ac.tuwien.sepm.assignment.individual.entities;

import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;

import java.time.LocalDate;

/**
 * Created by alija on 02.04.18
 */
public class BookedVehicle {

    private Integer bid;
    private Integer vid;
    private String name;
    private Double pricePerHour;
    private LocalDate licenseDate;
    private String licenseNumber;



    public BookedVehicle(Integer vid,String name) {
        this.vid = vid;
        this.name = name;
    }

    public BookedVehicle(){};

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    public Integer getVid() {
        return vid;
    }

    public void setVid(Integer vid) {
        this.vid = vid;
    }

    public LocalDate getLicenseDate() {
        return licenseDate;
    }

    public void setLicenseDate(LocalDate licenseDate) {
        this.licenseDate = licenseDate;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(Double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }
}
