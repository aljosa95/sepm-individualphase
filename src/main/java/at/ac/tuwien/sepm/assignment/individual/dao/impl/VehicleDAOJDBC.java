package at.ac.tuwien.sepm.assignment.individual.dao.impl;

import at.ac.tuwien.sepm.assignment.individual.dao.DAOException;
import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;
import at.ac.tuwien.sepm.assignment.individual.util.DBUtil;
import at.ac.tuwien.sepm.assignment.individual.dao.VehicleDAO;
import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.time.LocalTime;

/**
 * Created by alija on 19.03.18
 */
public class VehicleDAOJDBC implements VehicleDAO {


    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());


    @Override
    public void create(Vehicle v) throws DAOException {
        LOG.info("Adding vehicle to DB {}", v);
        try {
            String vehicleInsertString = "INSERT INTO VEHICLE (VID,NAME,BUILD_YEAR,DESCRIPTION,SEATS,TAG_NUMBER,TYPE_OF_DRIVE,POWER,PRICE_PER_HOUR,IMAGE,VEHICLE_CLASS,CREATED_DATE,CREATED_TIME,EDIT_DATE,EDIT_TIME) VALUES (default,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = DBUtil.getConnection().prepareStatement(vehicleInsertString, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,v.getName());
            ps.setInt(2,v.getBuildYear());
            ps.setString(3,v.getDescription());
            ps.setObject(4,v.getSeats());
            ps.setString(5,v.getTagNumber());
            ps.setString(6,v.getTypeOfDrive());
            ps.setObject(7,v.getPower());
            ps.setObject(8,v.getPricePerHour());
            ps.setString(9,v.getImg());
            ps.setObject(10,v.getVehicleClass().name());
            ps.setDate(11,new java.sql.Date(new java.util.Date().getTime()));
            ps.setTime(12,Time.valueOf(LocalTime.now().toString().substring(0,8)));
            ps.setDate(13,new java.sql.Date(new java.util.Date().getTime()));
            ps.setTime(14,Time.valueOf(LocalTime.now().toString().substring(0,8)));

            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            generatedKeys.next();
            v.setId(generatedKeys.getInt(1));
            LOG.info("Added to Database {}",v);

        } catch (NullPointerException | SQLException e) {
            LOG.error("SQL Exception at VehicleDAO by adding Vehicle");
            throw new DAOException(e.getMessage());
        }


    }

    @Override
    public ObservableList<Vehicle> findAll() throws DAOException {
        ObservableList<Vehicle> observableList = FXCollections.observableArrayList();
        try {
            PreparedStatement ps = DBUtil.getConnection().prepareStatement("SELECT * FROM VEHICLE WHERE isDeleted = FALSE ");
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Vehicle vehicle = new Vehicle();
                vehicle.setId(rs.getInt(1));
                vehicle.setName(rs.getString(2));
                vehicle.setBuildYear(rs.getInt(3));
                vehicle.setDescription(rs.getString(4));
                vehicle.setSeats(rs.getInt(5));
                vehicle.setTagNumber(rs.getString(6));
                vehicle.setTypeOfDrive(rs.getString(7));
                vehicle.setPower(rs.getLong(8));
                vehicle.setPricePerHour(rs.getDouble(9));
                vehicle.setVehicleClass( DriverLicense.valueOf(rs.getString(10)));
                vehicle.setImg(rs.getString(11));
                vehicle.setCreatedDate(rs.getDate(12).toLocalDate());
                vehicle.setCreateTime((rs.getTime(13)).toLocalTime());
                vehicle.setEditDate(rs.getDate(14).toLocalDate());
                vehicle.setEditTime(rs.getTime(15).toLocalTime());
                vehicle.setDeleted(rs.getBoolean(16));

                observableList.add(vehicle);
            }

        rs.close();
        } catch (SQLException e) {
            LOG.error("SQL Exception at VehicleDAO by loading vehicles");
            throw new DAOException(e.getMessage());
        }
        LOG.info("Vehicles loaded from DB");
        return observableList;
    }

    @Override
    public void update(Vehicle v) throws DAOException {
        String vehicleUpdateString = "UPDATE VEHICLE SET NAME=?,BUILD_YEAR=?,DESCRIPTION=?,SEATS=?,TAG_NUMBER=?,TYPE_OF_DRIVE=?,POWER=?,PRICE_PER_HOUR=?,IMAGE=?,VEHICLE_CLASS=?,EDIT_DATE=?,EDIT_TIME=? WHERE VID=?";
        LOG.info("Updating vehicle to DB {}", v);
        try {
            PreparedStatement checkPS = DBUtil.getConnection().prepareStatement("SELECT VID FROM BOOKEDVEHICLE");
            ResultSet rs = checkPS.executeQuery();
            boolean alreadyBooked = false;
            while (rs.next()){
                if (rs.getInt(1)==v.getId()){
                    int id = v.getId();
                    Vehicle vehicle = new Vehicle();
                    vehicle.setId(id);
                    delete(vehicle);
                    create(v);
                    alreadyBooked = true;
                    break;
                }
            }
            if (!alreadyBooked) {
                PreparedStatement ps = DBUtil.getConnection().prepareStatement(vehicleUpdateString, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, v.getName());
                ps.setInt(2, v.getBuildYear());
                ps.setString(3, v.getDescription());
                ps.setObject(4, v.getSeats());
                ps.setString(5, v.getTagNumber());
                ps.setString(6, v.getTypeOfDrive());
                ps.setObject(7, v.getPower());
                ps.setDouble(8, v.getPricePerHour());
                ps.setString(9, v.getImg());
                ps.setObject(10, v.getVehicleClass().name());
                ps.setDate(11, new java.sql.Date(new java.util.Date().getTime()));
                ps.setTime(12, Time.valueOf(LocalTime.now().toString().substring(0, 8)));
                ps.setInt(13, v.getId());

                ps.executeUpdate();
            }
            LOG.info("Updated to Database {}",v);

        } catch (NullPointerException | SQLException e) {
            LOG.error("SQL Exception at VehicleDAO by updating Vehicle");
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public void delete(Vehicle v) throws DAOException {
        String vehicleDeleteString = "UPDATE VEHICLE SET isDeleted=? WHERE VID=?";
        LOG.info("Deleting vehicle in DB {}", v);
        try {
            PreparedStatement ps = DBUtil.getConnection().prepareStatement(vehicleDeleteString, Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1,true);
            ps.setInt(2,v.getId());
            ps.executeUpdate();

            ps.executeUpdate();
            LOG.info("Deleted in Database {}",v);

        } catch (NullPointerException | SQLException e) {
            LOG.error("SQL Exception at VehicleDAO by deleting Vehicle");
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public ObservableList<Vehicle> searchVehicles(Vehicle vehicle) throws DAOException {
        ObservableList<Vehicle> vehicles = FXCollections.observableArrayList();
        if (vehicle==null){
            throw new DAOException("Vehicle been passed is null!");
        }
        String searchString = "SELECT DISTINCT v.name,v.vehicle_class,v.type_of_drive,v.price_per_hour,v.vid,v.image FROM VEHICLE v JOIN BOOKEDVEHICLE bv ON v.vid=bv.vid JOIN BOOKING b ON b.BID=bv.BID WHERE ";
        if (vehicle.getName() != null){
            searchString += "UPPER(v.name) like UPPER('%" + vehicle.getName() + "%') AND ";
        }
        if (vehicle.getSeats() != null){
            searchString += " v.seats = " + vehicle.getSeats() + " AND ";
        }
        if (vehicle.getMinPrice() != null){
            searchString += " v.price_per_hour >= " + vehicle.getMinPrice() + " AND ";
        }
        if (vehicle.getMaxPrice() != null){
            searchString += " v.price_per_hour <= " + vehicle.getMaxPrice() + " AND ";
        }
        if (vehicle.getTypeOfDrive() != null){
            searchString += " v.type_of_drive = '" + vehicle.getTypeOfDrive() + "' AND ";
        }
        if (vehicle.getVehicleClass() != null){
            searchString += " v.vehicle_class = '" + vehicle.getVehicleClass() + "' AND ";
        }
        if (!vehicle.isBookedVehicle() && vehicle.getBeginDate()==null){
            searchString += " v.VID NOT IN (SELECT bv.VID FROM BOOKEDVEHICLE bv JOIN BOOKING b ON bv.bid=b.bid WHERE b.status='open') AND ";
        }
        if (vehicle.isBookedVehicle() && vehicle.getBeginDate()==null){
            searchString += " v.VID IN (SELECT VID FROM BOOKEDVEHICLE bv JOIN BOOKING b ON bv.bid=b.bid WHERE b.status='open') AND ";
        }
        if (vehicle.isBookedVehicle() && vehicle.getBeginDate() != null){
            searchString +=  " ( ( (b.begin_date  BETWEEN  '" + vehicle.getBeginDate() + "' AND '" + vehicle.getEndDate() + "')  AND (b.end_date  BETWEEN  '" + vehicle.getBeginDate() + "' AND '" + vehicle.getEndDate() + "' ) ) OR ( ( '" + vehicle.getBeginDate() + "' BETWEEN b.begin_date AND b.end_date) OR ( '" + vehicle.getEndDate() + "' BETWEEN b.begin_date AND b.end_date) ) )  AND b.status IN ('open','billed') AND ";
            if (vehicle.isTimeFilter()) {
                searchString += " ( (b.begin_date = '" + vehicle.getBeginDate() + "'  AND  b.begin_date = '" + vehicle.getEndDate() + "' ) OR ";
                searchString += " (b.begin_date != '" + vehicle.getBeginDate() + "'  AND  b.begin_date = '" + vehicle.getEndDate() + "' ) OR ";
                searchString += " (b.end_date = '" + vehicle.getBeginDate() + "'  AND  b.end_date = '" + vehicle.getEndDate() + "' ) OR ";
                searchString += " (b.end_date = '" + vehicle.getBeginDate() + "'  AND  b.end_date != '" + vehicle.getEndDate() + "') ) = ";
                searchString += " ( (CAST(SUBSTRING(b.begin_time,1,2) as NUMERIC(2,0))) AND (CAST(SUBSTRING(b.end_time,1,2) as NUMERIC(2,0)))  AND '" + vehicle.getBeginTime().substring(0, 2) + "' >= b.begin_time AND '" + vehicle.getEndTime().substring(0,2) + "' <= b.end_time ) AND ";
            }
        }
        if (!vehicle.isBookedVehicle() && vehicle.getBeginDate() != null){
            searchString +=  " (b.begin_date NOT  BETWEEN  '" + vehicle.getBeginDate() + "' AND '" + vehicle.getEndDate() + "' )  AND (b.end_date NOT BETWEEN  '" + vehicle.getBeginDate() + "' AND '" + vehicle.getEndDate() + "')  AND b.status IN ('open','billed','canceled') AND ";
            if (vehicle.isTimeFilter()) {
                searchString += " ( (b.begin_date = '" + vehicle.getBeginDate() + "'  AND  b.begin_date = '" + vehicle.getEndDate() + "' ) OR ";
                searchString += " (b.begin_date != '" + vehicle.getBeginDate() + "'  AND  b.begin_date = '" + vehicle.getEndDate() + "' ) OR ";
                searchString += " (b.end_date = '" + vehicle.getBeginDate() + "'  AND  b.end_date = '" + vehicle.getEndDate() + "' ) OR ";
                searchString += " (b.end_date = '" + vehicle.getBeginDate() + "'  AND  b.end_date != '" + vehicle.getEndDate() + "') ) = ";
                searchString += " ( (CAST(SUBSTRING(b.begin_time,1,2) as NUMERIC(2,0))) AND (CAST(SUBSTRING(b.end_time,1,2) as NUMERIC(2,0)))  AND ('" + vehicle.getBeginTime().substring(0, 2) + "' < b.begin_time AND '" + vehicle.getEndTime().substring(0,2) + "' < b.begin_time) OR ('" + vehicle.getBeginTime().substring(0, 2) + "' > b.end_time AND '" + vehicle.getEndTime().substring(0,2) + "' > b.end_time) ) AND ";
            }
        }

        searchString += "isDeleted = false;";
        try {
            PreparedStatement ps = DBUtil.getConnection().prepareStatement(searchString);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Vehicle v = new Vehicle();
                v.setName(rs.getString(1));
                v.setVehicleClass((DriverLicense.valueOf(rs.getString(2))));
                v.setTypeOfDrive(rs.getString(3));
                v.setPricePerHour(rs.getDouble(4));
                v.setId(rs.getInt(5));
                v.setImg(rs.getString(6));
                vehicles.add(v);
            }
        } catch (SQLException e) {
            LOG.error("SQL Exception at VehicleDAO by searching Vehicle");
            throw new DAOException(e.getMessage());
        }
        return vehicles;
    }



}
