package at.ac.tuwien.sepm.assignment.individual.ui;

import at.ac.tuwien.sepm.assignment.individual.service.BookingService;
import at.ac.tuwien.sepm.assignment.individual.service.VehicleService;
import at.ac.tuwien.sepm.assignment.individual.util.DBUtil;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

public class MainController  {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private VehicleService vehicleService;
    private BookingService bookingService;
    private Stage primaryStage;


    @FXML
    private AnchorPane bookingsTab;
    @FXML
    private AnchorPane vehiclesTab;
    @FXML
    private VehiclesTabController vehiclesTabController;
    @FXML
    private BookingsTabController bookingsTabController;
    @FXML
    private CreateBookingController createBookingController;


    @FXML
    private void initialize(){
        LOG.debug("MainController initialized!");
    }

    public void loadData(){
        vehiclesTabController.setVehicleService(vehicleService);
        vehiclesTabController.fillVehicleTable();
        vehiclesTabController.setPrimaryStage(primaryStage);
        vehiclesTabController.setBookingService(bookingService);

        bookingsTabController.setBookingService(bookingService);
        bookingsTabController.setVehicleService(vehicleService);
        bookingsTabController.fillBookingTable();
        bookingsTabController.setPrimaryStage(primaryStage);

        vehiclesTabController.setBookingsTabController(bookingsTabController);

    }


    @FXML
    private void newVehicleClicked(ActionEvent actionEvent) throws IOException  {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/createVehicle.fxml"));
        Scene scene = new Scene(loader.load());
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        stage.setScene(scene);

        CreateVehicleController createVehicleController = loader.getController();
        createVehicleController.setCreationMode(true);
        createVehicleController.setStage(stage);
        createVehicleController.setVehicleService(vehicleService);
        createVehicleController.setVehiclesTabController(vehiclesTabController);
        stage.showAndWait();
    }


    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }



    public void setVehicleService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
        vehiclesTabController.setVehicleService(vehicleService);
    }

    @FXML
    private void newBookingClicked(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/createBooking.fxml"));
        Scene scene = new Scene(loader.load());
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        stage.setScene(scene);

        CreateBookingController createBookingController = loader.getController();
        createBookingController.setCreationMode(true);
        createBookingController.setStage(stage);
        createBookingController.setVehicleService(vehicleService);
        createBookingController.setBookingService(bookingService);
        createBookingController.setBookingsTabController(bookingsTabController);
        createBookingController.loadVehicles();
        stage.showAndWait();
    }

    @FXML
    private void onExitClicked(ActionEvent actionEvent) {
        DBUtil.closeConnection();
        LOG.debug("Application shutdown initiated");
        primaryStage.close();
    }

    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }
}