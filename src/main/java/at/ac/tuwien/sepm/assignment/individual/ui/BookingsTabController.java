package at.ac.tuwien.sepm.assignment.individual.ui;

import at.ac.tuwien.sepm.assignment.individual.entities.Booking;
import at.ac.tuwien.sepm.assignment.individual.service.BookingService;
import at.ac.tuwien.sepm.assignment.individual.service.ServiceException;
import at.ac.tuwien.sepm.assignment.individual.service.VehicleService;
import at.ac.tuwien.sepm.assignment.individual.ui.div.UserAlert;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.scene.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.time.LocalDate;
import java.util.Calendar;

/**
 * Created by alija on 05.04.18
 */
public class BookingsTabController {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private UserAlert userAlert = new UserAlert();

    private BookingService bookingService;
    private VehicleService vehicleService;

    @FXML
    private TableColumn<Booking, String> bnameColumn;
    @FXML
    private TableColumn<Booking,LocalDate> bdateColumn;
    @FXML
    private TableColumn<Booking,LocalDate> edateColumn;
    @FXML
    private TableColumn<Booking,String> btimeColumn;
    @FXML
    private TableColumn<Booking,String> etimeColumn;
    @FXML
    private TableColumn<Booking,String> statusColumn;
    @FXML
    private TableColumn<Booking,Long> priceColumn;
    @FXML
    private TableView<Booking> bookingTable;

    private ObservableList<Booking> bookings = FXCollections.observableArrayList();
    private Stage primaryStage;
    private Integer billNumber = -1;

    @FXML
    private void initialize(){
        bookingTable.setEditable(true);

        bnameColumn.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        bdateColumn.setCellValueFactory(new PropertyValueFactory<>("dateBegin"));
        edateColumn.setCellValueFactory(new PropertyValueFactory<>("dateEnd"));
        btimeColumn.setCellValueFactory(new PropertyValueFactory<>("timeBegin"));
        etimeColumn.setCellValueFactory(new PropertyValueFactory<>("timeEnd"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("totalPrice"));

        bookingTable.getSortOrder().add(bdateColumn);
        bookingTable.getSortOrder().add(btimeColumn);
    }


    public void fillBookingTable(){
        LOG.info("Loading all Bookings!");
        try {
            bookings = bookingService.findAll();
        } catch (ServiceException e) {
            LOG.error(e.getMessage());
        }

        bookingTable.setItems(bookings);
        bookingTable.getSortOrder().add(bdateColumn);
        bookingTable.getSortOrder().add(btimeColumn);
        if (billNumber==-1){
            for (Booking booking: bookings) {
                if ((booking.getStatus().equals("canceled")) || (booking.getStatus().equals("billed"))){
                    if (booking.getBillNumber()>billNumber) {
                        billNumber = booking.getBillNumber();
                    }
                }
            }
            if (billNumber==-1){
                billNumber = 101;
            }
            else ++billNumber;

        }
    }


    @FXML
    private void cancelClicked(ActionEvent actionEvent) {
        Booking booking = bookingTable.getSelectionModel().getSelectedItem();
        booking.setBillNumber(billNumber);
        billNumber++;

        if (bookingTable.getSelectionModel().isEmpty()){
            userAlert.showWarning("Please select one booking/bill!");
        }
        else if (!booking.getStatus().equals("open")){
            userAlert.showWarning("Only open bills can be canceled!");
        }
        else {
            try {
                bookingService.cancelBooking(booking);
                LOG.info("Booking canceled for: " + booking.getCustomerName());
                userAlert.showInfo("Booking canceled for: " + booking.getCustomerName());
                fillBookingTable();
            } catch (ServiceException e) {
                LOG.error("Cancellation booking error: " + e.getMessage());
                userAlert.showWarning(e.getMessage());
            }
        }
    }

    @FXML
    private void detailsClicked(ActionEvent actionEvent) {
        if (bookingTable.getSelectionModel().getSelectedItems().size() > 1){
            userAlert.showInfo("Please select one booking!");
        }
        else if (bookingTable.getSelectionModel().getSelectedItem() == null){
            userAlert.showInfo("Please select one booking!");
        }
        else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/detailsBooking.fxml"));
            Scene scene = null;
            try {
                scene = new Scene(loader.load());
            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(scene);

            CreateBookingController bookingController = loader.getController();
            bookingController.setCreationMode(false);
            bookingController.setStage(stage);
            bookingController.setBookingService(bookingService);
            bookingController.setVehicleService(vehicleService);
            bookingController.setBookingsTabController(this);
            bookingController.setBooking(bookingTable.getSelectionModel().getSelectedItem());
            bookingController.loadData();
            stage.showAndWait();
            fillBookingTable();
        }

    }

    @FXML
    private void billBookingClicked(ActionEvent actionEvent) {
        Booking booking = bookingTable.getSelectionModel().getSelectedItem();
        booking.setBillNumber(billNumber);
        billNumber++;

        if (bookingTable.getSelectionModel().isEmpty()){
            userAlert.showWarning("Please select one booking/bill!");
        }
        else if (!booking.getStatus().equals("open")){
            userAlert.showWarning("Only open bills can be billed!");
        }
        else {
            try {
                bookingService.billBooking(booking);
                LOG.info("Booking billed for: " + booking.getCustomerName());
                userAlert.showInfo("Booking billed for: " + booking.getCustomerName());
                fillBookingTable();
            } catch (ServiceException e) {
                LOG.error("Billing booking error: " + e.getMessage());
                userAlert.showWarning(e.getMessage());
            }
        }
    }




    public BookingService getBookingService() {
        return bookingService;
    }

    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setVehicleService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }
}
