package at.ac.tuwien.sepm.assignment.individual.dao;


import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import at.ac.tuwien.sepm.assignment.individual.service.ServiceException;
import javafx.collections.ObservableList;

/**
 * Created by Alija on 19.03.2018
 */
public interface VehicleDAO  {

    /**
     * Inserting new vehicle in database
     * @param vehicle object that we want to create and put in DB
     * @throws DAOException gives database exception
     */
    void create(Vehicle vehicle) throws DAOException;


    /**
     * List all vehicles at the begin of program by querying the datebase
     * @return Observable list of all vehicles from DB
     * @throws DAOException gives database exception
     */
    ObservableList<Vehicle> findAll() throws DAOException;

    /**
     * Update vehicle in database that we edited sending respective queries
     * @param v vehicle that has been edited
     * @throws DAOException gives database exception
     */
    void update(Vehicle v) throws DAOException;


    /**
     * Delete vehicle from system sending respective queries
     * @param v vehicle that should be deleted
     * @throws DAOException gives database exception
     */
    void delete(Vehicle v) throws DAOException;

    /**
     * Search for all Vehicles in database that fulfill search filters
     * @param vehicle object where all are search filters saved
     * @return list of vehicles that we want to output after search
     * @throws DAOException thrown when search query cannot be smoothly preformed
     */
    ObservableList<Vehicle> searchVehicles(Vehicle vehicle) throws DAOException;
}
