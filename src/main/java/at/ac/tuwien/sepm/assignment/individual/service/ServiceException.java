package at.ac.tuwien.sepm.assignment.individual.service;

/**
 * Created by alija on 19.03.18
 */
public class ServiceException extends Exception{

    public ServiceException(){
        super();
    }

    public ServiceException(String msg){
        super(msg);
    }
}
