package at.ac.tuwien.sepm.assignment.individual.ui;

import at.ac.tuwien.sepm.assignment.individual.entities.BookedVehicle;
import at.ac.tuwien.sepm.assignment.individual.entities.Booking;
import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import at.ac.tuwien.sepm.assignment.individual.service.BookingService;
import at.ac.tuwien.sepm.assignment.individual.service.ServiceException;
import at.ac.tuwien.sepm.assignment.individual.service.VehicleService;
import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;
import at.ac.tuwien.sepm.assignment.individual.ui.div.SpinnerValue;
import at.ac.tuwien.sepm.assignment.individual.ui.div.UserAlert;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;
import java.util.Optional;


import java.lang.invoke.MethodHandles;
import java.util.Date;

/**
 * Created by alija on 31.03.18
 */
public class CreateBookingController {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private UserAlert userAlert = new UserAlert();
    private VehicleService vehicleService;
    private BookingService bookingService;
    private Stage stage;
    private boolean creationMode;
    private BookingsTabController bookingsTabController;


    @FXML
    private javafx.scene.control.TextField tf_cardNumber,tf_name;
    @FXML
    private DatePicker dp_bdate,dp_edate;
    @FXML
    private Spinner<LocalTime> s_btime;
    @FXML
    private Spinner<LocalTime> s_etime;

    public TableView<Vehicle> getVehicleTable() {
        return vehicleTable;
    }

    public void setVehicleTable(TableView<Vehicle> vehicleTable) {
        this.vehicleTable = vehicleTable;
    }

    @FXML
    private TableView<Vehicle> vehicleTable,bookingTable;
    @FXML
    private TableColumn<Vehicle,String> vnameColumn,bnameColumn;
    @FXML
    private TableColumn<Vehicle,DriverLicense> vclassColumn,bclassColumn;
    @FXML
    private TableColumn<Vehicle,String> vtypeColumn,btypeColumn;
    @FXML
    private TableColumn<Vehicle,Long> vpriceColumn,bpriceColumn;

    private TextField tf_licenseNumber = new TextField();

    private DatePicker tf_licenseDate = new DatePicker();

    @FXML
    private Group licenseGroup;
    @FXML
    private DialogPane licenseDialog;
    @FXML
    private Button saveLicenseData;
    @FXML
    private Button editButton;
    @FXML
    private TextField tf_status;
    @FXML
    private TextField tf_createTime;
    @FXML
    private Group editBookingGroup;
    @FXML
    private Button saveButton;
    @FXML
    private TextField tf_totalPrice,tf_billNumber,tf_billTime;
    @FXML
    private TableColumn <BookedVehicle,LocalDate>licenseDateColumn;
    @FXML
    private TableColumn<BookedVehicle,String> licenseNumberColumn;
    @FXML
    private TableColumn<BookedVehicle,Double> vpriceColumnEdit;
    @FXML
    private TableColumn<BookedVehicle,String> vnameColumnEdit;
    @FXML
    private TableView<BookedVehicle> detailTable;
    @FXML
    private Group detailsGroup,billGroup;

    private ObservableList<Vehicle> vehiclesList = FXCollections.observableArrayList();
    private ObservableList<BookedVehicle> bookedVehicles = FXCollections.observableArrayList();

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    private Booking booking;



    @FXML
    private void initialize(){
        vehicleTable.setEditable(true);
        vehicleTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        bookingTable.setEditable(true);
        bookingTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        vnameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        vclassColumn.setCellValueFactory(new PropertyValueFactory<>("vehicleClass"));
        vpriceColumn.setCellValueFactory(new PropertyValueFactory<>("pricePerHour"));
        vtypeColumn.setCellValueFactory(new PropertyValueFactory<>("typeOfDrive"));

        bnameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        bclassColumn.setCellValueFactory(new PropertyValueFactory<>("vehicleClass"));
        bpriceColumn.setCellValueFactory(new PropertyValueFactory<>("pricePerHour"));
        btypeColumn.setCellValueFactory(new PropertyValueFactory<>("typeOfDrive"));


        s_btime.setValueFactory(new SpinnerValue().getStartSpinner());
        s_etime.setValueFactory(new SpinnerValue().getStartSpinner());


    }

    public void loadVehicles(){
        try {
            vehiclesList = vehicleService.findAll();
            vehicleTable.setItems(vehiclesList);
        } catch (ServiceException e) {
            LOG.error(e.getMessage());
        }
    }


    @FXML
    private void onExitClicked(ActionEvent actionEvent) {
        if (!tf_name.getText().equals("") || !tf_cardNumber.getText().equals("") || (bookingTable.getItems()!=null)){
            if(userAlert.showConfirmation("Are you sure want to exit?")){
                stage.close();
            }
        }
        else {
            stage.close();
        }
    }

    @FXML
    private void saveBooking(ActionEvent actionEvent) {
        if (tf_name.getText().equals("")  || tf_cardNumber.getText().equals("") || dp_bdate.getValue()==null || dp_edate.getValue()==null){
            userAlert.showWarning("Not all obligatory fields provided!");
            LOG.info("Obligatory fields for booking are missing!");
        }
        else {
            LocalDate bookDate = dp_bdate.getValue();
            LocalDate dateNow = LocalDate.now();
            int days = Period.between(dateNow,bookDate).getDays();
            Integer bookTime = Integer.parseInt(s_btime.getValue().toString().substring(0,2));
            Integer hourNow = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            int diff = hourNow-bookTime;
            if ((days<7 || (days==7 && diff>=0))){
                if (!userAlert.showConfirmation("Free canceling would not be possible, are you sure that you want to book?")) {
                    return;
                }
            }
            if(creationMode){
                booking = new Booking(tf_name.getText(),tf_cardNumber.getText(),dp_bdate.getValue(),dp_edate.getValue(),s_btime.getValue().toString(),s_etime.getValue().toString());
                booking.setBookedVehicles(bookedVehicles);
                try {
                    bookingService.saveBooking(booking);
                    userAlert.showInfo("Booking created successfully!");
                    bookingsTabController.fillBookingTable();
                    this.stage.close();
                } catch (ServiceException e) {
                    userAlert.showWarning(e.getMessage());
                    LOG.error(e.getClass().getSimpleName() + " " + e.getMessage());
                    bookingsTabController.fillBookingTable();
                }
            }
            else {
                booking.setBookedVehicles(bookedVehicles);
                booking.setCustomerName(tf_name.getText());
                booking.setCardNumber(tf_cardNumber.getText());
                booking.setDateBegin(dp_bdate.getValue());
                booking.setDateEnd(dp_edate.getValue());
                booking.setTimeBegin(s_btime.getValue().toString());
                booking.setTimeEnd(s_etime.getValue().toString());
                try {
                    bookingService.updateBooking(booking);
                    userAlert.showInfo("Booking updated successfully!");
                    bookingsTabController.fillBookingTable();
                    stage.close();
                } catch (ServiceException e) {
                    LOG.error(e.getClass().getSimpleName() + " " + e.getMessage());
                    userAlert.showWarning(e.getMessage());
                }
            }
        }

    }

    @FXML
    private void addToBookingClicked(ActionEvent actionEvent) {
        if (bookingTable.getItems() == null) {
            userAlert.showInfo("Plese select one or more vehicle(s)!");
        } else {
            ObservableList<Vehicle> vehicles, vehiclesInOrder;
            vehiclesInOrder = bookingTable.getItems();
            vehicles = vehicleTable.getSelectionModel().getSelectedItems();
            for (Vehicle vehicle : vehicles) {
                if (!vehiclesInOrder.contains(vehicle)) {
                    if (!vehicle.getVehicleClass().equals(DriverLicense.NOLICENSE)) {
                        licenseDialog(vehicle);
                        if (tf_licenseDate.getValue() == null ||  tf_licenseNumber.getText().equals("")){
                           return;
                        }
                        if (vehicle.getVehicleClass().toString().contains("A") || vehicle.getVehicleClass().toString().contains("C")) {
                            LocalDate license = tf_licenseDate.getValue();
                            LocalDate now = LocalDate.now();
                            int period = Period.between(license,now).getYears();
                            if (period < 3){
                                userAlert.showWarning("License Date of Issue for this Vehicle has to be at least 3 years old!");
                                LOG.info("User License is not valid for this type of Vehicle!");
                                return;
                            }
                        }
                        bookingTable.getItems().add(vehicle);
                    }
                    else{ bookingTable.getItems().add(vehicle);}
                    if (bookingTable.getItems().contains(vehicle)){
                        BookedVehicle bookedVehicle = new BookedVehicle(vehicle.getId(),vehicle.getName());
                        bookedVehicle.setPricePerHour(vehicle.getPricePerHour());
                        if (!vehicle.getVehicleClass().equals(DriverLicense.NOLICENSE)){
                            bookedVehicle.setLicenseDate((tf_licenseDate.getValue()));
                            bookedVehicle.setLicenseNumber(tf_licenseNumber.getText());
                        }
                        bookedVehicles.add(bookedVehicle);
                    }
                } else {
                    userAlert.showInfo(vehicle.getName() + " is already in booking!");
                }
            }
        }
    }

    private void licenseDialog(Vehicle vehicle) {
        tf_licenseNumber.clear();
        Dialog<Pair<String, DatePicker>> dialog = new Dialog<>();
        dialog.setTitle("License Data for: " + vehicle.getName());

        ButtonType saveButton = new ButtonType("Save", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL,saveButton);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(50);
        gridPane.setVgap(50);
        gridPane.setPadding(new Insets(20, 150, 10, 10));

        gridPane.add(new Label("*License Number:"), 0, 0);
        gridPane.add(tf_licenseNumber, 1, 0);
        gridPane.add(new Label("*License Date of Issue:"), 2, 0);
        gridPane.add(tf_licenseDate, 3, 0);

        dialog.getDialogPane().setContent(gridPane);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == saveButton) {
                if ( (tf_licenseNumber.getText().equals("")) || tf_licenseDate.getValue() == null){
                    userAlert.showInfo("You need to insert both data!");
                }
                else return new Pair<String, DatePicker>(tf_licenseNumber.getText(), tf_licenseDate);
            }
            return null;
        });

        Optional<Pair<String, DatePicker>> result = dialog.showAndWait();

        result.ifPresent(pair -> {
        });

    }

    @FXML
    private void removeFromBookingClicked(ActionEvent actionEvent) {
        ObservableList<Vehicle> vehicles;
        vehicles = bookingTable.getSelectionModel().getSelectedItems();
        ObservableList<BookedVehicle> help = FXCollections.observableArrayList();
        //bookingTable.getItems().removeAll(vehicles);
        for (Vehicle v: vehicles) {
            for (BookedVehicle bv:bookedVehicles){
                if (v.getId().equals(bv.getVid())){
                    help.add(bv);
                }
            }
        }
        bookedVehicles.removeAll(help);
        bookingTable.getItems().removeAll(vehicles);
    }

    @FXML
    private void onEditClicked(ActionEvent actionEvent) {
        saveButton.setDisable(false);
        tf_name.setEditable(true);
        tf_cardNumber.setEditable(true);
        dp_edate.setEditable(true);
        dp_bdate.setEditable(true);
        s_etime.setEditable(true);
        s_btime.setEditable(true);
        detailsGroup.setVisible(false);
        editBookingGroup.setVisible(true);
        ObservableList<Vehicle> helpList = FXCollections.observableArrayList();
        loadVehicles();
        for (BookedVehicle bv: booking.getBookedVehicles()) {
            for (Vehicle  v: vehiclesList) {
                if (v.getId().equals(bv.getVid())){
                   helpList.add(v);
                }
            }
        }
        bookingTable.setItems(helpList);
        vehicleTable.setItems(vehiclesList);
    }


    public void setVehicleService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public void setCreationMode(boolean creationMode) {
        this.creationMode = creationMode;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        if (creationMode) {
            stage.setTitle("Add New Booking");
        }
        else {
            stage.setTitle("Details/Edit View");
        }
    }


    public BookingsTabController getBookingsTabController() {
        return bookingsTabController;
    }

    public void setBookingsTabController(BookingsTabController bookingsTabController) {
        this.bookingsTabController = bookingsTabController;
    }


    public void loadData() {
        saveButton.setDisable(true);
        if (!booking.getStatus().equals("open")){
            editButton.setDisable(true);
            billGroup.setVisible(true);
            tf_billNumber.setText(booking.getBillNumber().toString());
            tf_billTime.setText(booking.getBillTimestamp().toString().substring(0,16));
        }
        detailTable.setEditable(false);
        vnameColumnEdit.setCellValueFactory(new PropertyValueFactory<>("name"));
        vpriceColumnEdit.setCellValueFactory(new PropertyValueFactory<>("pricePerHour"));
        licenseDateColumn.setCellValueFactory(new PropertyValueFactory<>("licenseDate"));
        licenseNumberColumn.setCellValueFactory(new PropertyValueFactory<>("licenseNumber"));

        tf_name.setText(booking.getCustomerName());
        tf_cardNumber.setText(booking.getCardNumber());
        dp_bdate.setValue(booking.getDateBegin());
        dp_edate.setValue(booking.getDateEnd());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        s_btime.getValueFactory().setValue(LocalTime.parse(booking.getTimeBegin()));
        s_etime.getValueFactory().setValue(LocalTime.parse(booking.getTimeEnd()));
        tf_totalPrice.setText(booking.getTotalPrice().toString());
        detailTable.setItems(booking.getBookedVehicles());
        tf_createTime.setText(booking.getCreateTimestamp().toString().substring(0,16));
        tf_status.setText(booking.getStatus());
        bookedVehicles = booking.getBookedVehicles();
    }


}
