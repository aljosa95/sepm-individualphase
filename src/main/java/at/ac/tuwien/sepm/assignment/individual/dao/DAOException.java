package at.ac.tuwien.sepm.assignment.individual.dao;

/**
 * Created by alija on 19.03.18
 */
public class DAOException extends Exception {

    public DAOException(){
        super();
    }

    public DAOException(String msg){
        super(msg);
    }
}
