package at.ac.tuwien.sepm.assignment.individual.ui.div;

import javafx.scene.control.SpinnerValueFactory;
import javafx.util.converter.LocalTimeStringConverter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by alija on 02.04.18
 */
public class SpinnerValue {

    private SpinnerValueFactory<LocalTime> startSpinner;

    public SpinnerValue() {
        String pattern = "HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

         startSpinner = new SpinnerValueFactory<>() {
            {
                setConverter(new LocalTimeStringConverter(formatter,null));
                setValue(LocalTime.of(12,0));
            }
            @Override
            public void decrement(int steps) {
                LocalTime time = (LocalTime) getValue();
                setValue(time.minusHours(steps));
            }

            @Override
            public void increment(int steps) {
                LocalTime time = (LocalTime) getValue();
                setValue(time.plusHours(steps));
            }
        };
    }



    public SpinnerValueFactory<LocalTime> getStartSpinner() {
        return startSpinner;
    }
}
