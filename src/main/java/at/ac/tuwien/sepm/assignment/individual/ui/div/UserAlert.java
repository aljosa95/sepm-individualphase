package at.ac.tuwien.sepm.assignment.individual.ui.div;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Created by alija on 01.04.18
 */
public class UserAlert {


    public  void showWarning(String s) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("User Warning");
        alert.setHeaderText((String) null);
        alert.setContentText(s);
        alert.showAndWait();
    }

    public void showInfo(String msg) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText((String) null);
        alert.setContentText(msg);
        alert.showAndWait();
    }

    public boolean showConfirmation(String s) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(s);
        alert.setContentText("");
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }
}
