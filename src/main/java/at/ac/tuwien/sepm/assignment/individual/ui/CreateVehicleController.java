package at.ac.tuwien.sepm.assignment.individual.ui;

import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import at.ac.tuwien.sepm.assignment.individual.service.BookingService;
import at.ac.tuwien.sepm.assignment.individual.service.ServiceException;
import at.ac.tuwien.sepm.assignment.individual.service.VehicleService;
import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;
import at.ac.tuwien.sepm.assignment.individual.ui.div.UserAlert;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by alija on 21.03.18
 */
public class CreateVehicleController {


    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private UserAlert userAlert = new UserAlert();

    private VehicleService vehicleService;
    private Stage stage;
    private Vehicle vehicle = null;
    private Boolean creationMode;

    @FXML
    public Group imgUpload;
    @FXML
    private TextField tf_buildYear,tf_name,tf_seats,tf_tagNumber,tf_pricePerHour,tf_power,tf_imgPath;
    @FXML
    private TextArea td_description;
    @FXML
    private CheckBox musclePower;
    @FXML
    private CheckBox motorized;
    @FXML
    private Group motorPower;
    @FXML
    private Group licenseGroup;
    @FXML
    private CheckBox classC;
    @FXML
    private CheckBox classB;
    @FXML
    private CheckBox classA;
    @FXML
    private CheckBox licenseRequired;
    @FXML
    private VehiclesTabController vehiclesTabController;
    @FXML
    private Button saveButton;
    @FXML
    private ImageView vehicleImg;
    @FXML
    private TextField tf_createDate,tf_editDate;

    public BookingsTabController getBookingsTabController() {
        return bookingsTabController;
    }

    public void setBookingsTabController(BookingsTabController bookingsTabController) {
        this.bookingsTabController = bookingsTabController;
    }

    private BookingsTabController bookingsTabController;

    public BookingService getBookingService() {
        return bookingService;
    }

    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    private BookingService bookingService;


    public void vehicleIsMotorised() {
        if (motorized.isSelected()) {
            motorPower.setVisible(true);
            musclePower.setSelected(false);
        } else {
            motorPower.setVisible(false);
            musclePower.setSelected(false);
        }
    }

    public void saveVehicle(ActionEvent actionEvent) {
        boolean vehicleMade = true;
        // check obligatory fields
        if (!(tf_buildYear.getText().equals("")) && !(tf_name.getText().equals("")) && !(tf_pricePerHour.getText().equals("")) && (musclePower.isSelected() != motorized.isSelected() && isNumeric(tf_buildYear.getText()) && isNumeric(tf_pricePerHour.getText()))) {
            String typeOfDrive = motorized.isSelected() ? "motorized" : "muscle-powered";
            if (creationMode) {
                vehicle = new Vehicle(tf_name.getText(), Integer.parseInt(tf_buildYear.getText()), typeOfDrive, Double.parseDouble(tf_pricePerHour.getText()));
            }
            else {
                vehicle.setName(tf_name.getText());
                vehicle.setBuildYear(Integer.parseInt(tf_buildYear.getText()));
                vehicle.setTypeOfDrive(typeOfDrive);
                vehicle.setPricePerHour(Double.parseDouble(tf_pricePerHour.getText()));
            }
            // check optional fields
            if (!tf_seats.getText().equals("")) {
                if (isNumeric(tf_seats.getText())) {
                    vehicle.setSeats(Integer.parseInt(tf_seats.getText()));
                }
                else {
                    vehicleMade = false;
                }
            }
            if (motorized.isSelected()){
                if (isNumeric(tf_power.getText())) {
                    vehicle.setPower(Long.parseLong(tf_power.getText()));
                }
                else {
                    vehicleMade = false;
                }
            }
            if (licenseRequired.isSelected()){
                if (!tf_tagNumber.getText().equals("")) {
                    vehicle.setTagNumber(tf_tagNumber.getText());
                    setLicenseGroup(vehicle);
                    if (vehicle.getVehicleClass() == null) {
                        vehicleMade = false;
                    }
                }
                else {
                    vehicleMade = false;
                }
            }
            if (!tf_imgPath.getText().equals("")) {
                vehicle.setImg(tf_imgPath.getText());
            }
            if (!licenseRequired.isSelected()){
                vehicle.setVehicleClass(DriverLicense.NOLICENSE);
            }
            if (vehicleMade){
                if (creationMode) {
                    if (!td_description.getText().equals("")) {
                        vehicle.setDescription(td_description.getText());
                    }
                    /*vehicle.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                    vehicle.setEditDate(new java.sql.Date(new java.util.Date().getTime()));*/
                    try {
                        vehicleService.saveVehicle(vehicle);
                        userAlert.showInfo( "Vehicle has been successfully saved!");
                        //stage.close();
                        LOG.debug("vehicleID= " + vehicle.getId());
                        vehiclesTabController.fillVehicleTable();
                    } catch (ServiceException e) {
                        LOG.error(e.getClass().getSimpleName() + " " + e.getMessage());
                        userAlert.showWarning(e.getMessage());
                    }
                }
                else{
                    if (td_description.getText()==null || td_description.getText().equals("")) {
                        vehicle.setDescription(null);
                    }
                    else vehicle.setDescription(td_description.getText());
                    //vehicle.setEditDate(new java.sql.Date(new java.util.Date().getTime()));
                    try {
                        vehicleService.updateVehicle(vehicle);
                        userAlert.showInfo("Vehicle edited successfully!");
                        vehiclesTabController.fillVehicleTable();
                    } catch (ServiceException e) {
                        LOG.error(e.getClass().getSimpleName() + " " + e.getMessage());
                        userAlert.showWarning(e.getMessage());
                    }
                }
            }
            else{
                LOG.info("Not all fields provided!");
                userAlert.showWarning("Not all obligatory fields are provided!");
            }
        }
        else {
            LOG.info("Not all fields provided!");
            userAlert.showWarning("Not all obligatory fields are provided!");
        }

    }

    private void setLicenseGroup(Vehicle vehicle) {
        if (classA.isSelected()){
            vehicle.setVehicleClass(DriverLicense.A);
            if (classB.isSelected()){
                vehicle.setVehicleClass(DriverLicense.AB);
                if (classC.isSelected()){
                    vehicle.setVehicleClass(DriverLicense.ABC);
                }
            }
            else if (classC.isSelected()){
                vehicle.setVehicleClass(DriverLicense.AC);
            }
        }
        else if (classB.isSelected()){
            vehicle.setVehicleClass(DriverLicense.B);
            if (classC.isSelected()){
                vehicle.setVehicleClass(DriverLicense.BC);
            }
        }
        else if (classC.isSelected()){
            vehicle.setVehicleClass(DriverLicense.C);
        }
        else {
            vehicle.setVehicleClass(null);
            userAlert.showWarning("You must select at least one class!");
            LOG.info("Vehicle class has not been selected");
        }
    }

    private boolean isNumeric(String str) {
        Integer i;
        Double d;
        try {
            if (!str.contains(".")) {
                i = Integer.parseInt(str);
            }
            else {
                d = Double.parseDouble(str);
            }
        } catch (NumberFormatException e) {
            userAlert.showWarning("Values of number fields must be a number!");
            LOG.error("Invalid input in text field(s)");
            return false;
        }
        return true;
    }


    @FXML
    private void musclePowerAction(ActionEvent actionEvent) {
        if (musclePower.isSelected()) {
            musclePower.setSelected(true);
            motorPower.setVisible(false);
            motorized.setSelected(false);
            tf_power.clear();
        } else {
            musclePower.setSelected(false);
        }
    }

    private void clearFields(Boolean vehicleMade) {
        if (vehicleMade) {
            tf_seats.clear();
            tf_power.clear();
            tf_pricePerHour.clear();
            tf_name.clear();
            tf_buildYear.clear();
            td_description.clear();
            tf_tagNumber.clear();
            tf_imgPath.clear();
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        if (creationMode) {
            stage.setTitle("Add New Vehicle");
        }
        else {
            stage.setTitle("Details/Edit View");
        }
    }


    public void setCreationMode(boolean creationMode) {
        this.creationMode = creationMode;

    }

    @FXML
    private void onExitClicked(ActionEvent actionEvent) {
        if (!(tf_buildYear.getText().equals("")) || !(tf_name.getText().equals("")) || !(tf_pricePerHour.getText().equals("")) || !(tf_tagNumber.getText().equals("")) || !(tf_seats.getText().equals("")) || !(td_description.getText().equals(""))) {
            if (userAlert.showConfirmation("Are you sure you want to go back?")) {
                stage.close();
            }
            return;
        }
        stage.close();
    }


    @FXML
    public void findImgClicked(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose image");
        FileChooser.ExtensionFilter allowedTypes = new FileChooser.ExtensionFilter("Images types", "*.jpeg", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(allowedTypes);
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            tf_imgPath.setText(file.getAbsolutePath());
        }
    }

    @FXML
    public void onLicenseRequiredClicked(ActionEvent actionEvent) {
        if (licenseGroup.isVisible()) {
            licenseGroup.setVisible(false);
            tf_tagNumber.clear();
            classA.setSelected(false);
            classB.setSelected(false);
            classC.setSelected(false);
        } else {
            licenseGroup.setVisible(true);
        }
    }

    public void setVehiclesTabController(VehiclesTabController vehiclesTabController) {
        this.vehiclesTabController = vehiclesTabController;
    }

    public void setVehicleService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @FXML
    private void onEditClicked(ActionEvent actionEvent) {
        saveButton.setDisable(false);
        imgUpload.setVisible(true);
        vehicleImg.setVisible(false);
        classGroup.setVisible(true);
        classGroupDetail.setVisible(false);
        tf_name.setEditable(true);
        tf_buildYear.setEditable(true);
        tf_seats.setEditable(true);
        tf_tagNumber.setEditable(true);
        tf_power.setEditable(true);
        tf_pricePerHour.setEditable(true);
        td_description.setEditable(true);
        motorized.setDisable(false);
        musclePower.setDisable(false);
        licenseRequired.setDisable(false);
        requiredFields.setVisible(true);
        namelabel.setText("*"+namelabel.getText());
        yearlabel.setText("*"+yearlabel.getText());
        hourlabel.setText("*"+hourlabel.getText());
        taglabel.setText("*"+taglabel.getText());
        powerlabel.setText("*"+powerlabel.getText());
        vehicleImg.getParent().setVisible(false);
    }

    @FXML
    private void onAddToBookingClicked(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/createBooking.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(loader.load());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(this.stage);
        stage.setScene(scene);

        CreateBookingController createBookingController = loader.getController();
        createBookingController.setCreationMode(true);
        createBookingController.setStage(stage);
        createBookingController.setVehicleService(vehicleService);
        createBookingController.setBookingService(bookingService);
        createBookingController.setBookingsTabController(bookingsTabController);
        createBookingController.getVehicleTable().setItems(FXCollections.observableList(Collections.singletonList(vehicle)));
        stage.showAndWait();
    }

    @FXML
    private void onDeleteClicked(ActionEvent actionEvent){
        if (userAlert.showConfirmation("Are you sure that you want to delete vehicle " + vehicle.getName())){
            try {
                vehicleService.deleteVehicle(vehicle);
                vehiclesTabController.fillVehicleTable();
            } catch (ServiceException e) {
                LOG.error(e.getClass().getSimpleName() + " " + e.getMessage());
                userAlert.showWarning(e.getMessage());
            }
        }

    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @FXML
    private Group classGroup;
    @FXML
    private Group classGroupDetail;
    @FXML
    private TextField tf_vehicleClass;
    @FXML
    private Label requiredFields,namelabel,yearlabel,hourlabel,taglabel,powerlabel;

    public void loadData() throws MalformedURLException {
        tf_name.setText(vehicle.getName());
        tf_buildYear.setText(vehicle.getBuildYear().toString());
        td_description.setText(vehicle.getDescription());
        tf_pricePerHour.setText(vehicle.getPricePerHour().toString());
        tf_seats.setText(vehicle.getSeats().toString());
        if (vehicle.getTypeOfDrive().equals("motorized")){
            motorized.setSelected(true);
            tf_power.setText(vehicle.getPower().toString());

        }
        else{
            musclePower.setSelected(true);
        }
        motorized.setDisable(true);
        musclePower.setDisable(true);
        tf_vehicleClass.setText(vehicle.getVehicleClass().toString());
        if (!vehicle.getVehicleClass().toString().contains("NO")){
            licenseRequired.setSelected(true);
            tf_tagNumber.setText(vehicle.getTagNumber());
        }
        licenseRequired.setDisable(true);
        if (vehicle.getImg() != null) {
            File path = new File(vehicle.getImg());
            Image img = new Image(path.toURI().toURL().toExternalForm());
            vehicleImg.setPreserveRatio(true);
            vehicleImg.setImage(img);
            vehicleImg.setVisible(true);
        }
        tf_createDate.setText(vehicle.getCreatedDate() + " " + vehicle.getCreateTime());
        tf_editDate.setText(vehicle.getEditDate() + " " + vehicle.getEditTime());
        LOG.debug("Data loaded for Details View of Vehicle!");
    }
}
