package at.ac.tuwien.sepm.assignment.individual.service;

import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import javafx.collections.ObservableList;

/**
 * Created by alija on 19.03.18
 */
public interface VehicleService {

    /**
     * Called at adding new vehicle in database
     * @param v object of vehicle that we want to save
     * @throws ServiceException return Exception for Validation:
     * - if user name is too long,
     * - if price, seats and other number values are < 0 or have not realistic value,
     * - if build year is not in valid range
     * - if img cannot be validated, size and memory capacity
     * - if motorized vehicles do not have License...
     */
    void saveVehicle(Vehicle v) throws ServiceException;


    /**
     * List all vehicles at the begin of program
     * @return Observable list of all vehicles from DB
     * @throws ServiceException exception thrown for DB failures during loading
     */
    ObservableList<Vehicle> findAll() throws ServiceException;

    /**
     * Update vehicle in database that we edited
     * @param v vehicle that has been edited
     * @throws ServiceException give Exception for Validation same as
     * at Method  @saveVehicle
     *
     */
    void updateVehicle(Vehicle v) throws ServiceException;

    /**
     * Delete vehicle from system
     * @param v vehicle that should be deleted
     * @throws ServiceException give Exception for Deletion
     */
    void deleteVehicle(Vehicle v) throws ServiceException;

    /**
     * Delete all vehicles that have be selected in vehicles tab
     * @param vehicles list of vehicles to be deleted
     * @throws ServiceException give Exception for Deletion if deletion cannot be preformed in Database
     */
    void deleteAllVehicles(ObservableList<Vehicle> vehicles) throws ServiceException;

    /**
     * Search for specific vehicles as user give some properties
     * @param vehicle object that contains all filters of this search
     * @return list of vehicles that we want to output after search
     * @throws ServiceException give Exception when search filter cannot be validated:
     * -if beginDate/beginTime of Filter is after endDate/endTime,
     * -if numeric values (like seats,price) are not in valid range (less then 0)
     */
    ObservableList<Vehicle> searchVehicles(Vehicle vehicle) throws ServiceException;
}
