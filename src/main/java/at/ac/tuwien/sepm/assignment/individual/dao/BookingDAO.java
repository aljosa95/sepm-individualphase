package at.ac.tuwien.sepm.assignment.individual.dao;

import at.ac.tuwien.sepm.assignment.individual.entities.Booking;
import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import at.ac.tuwien.sepm.assignment.individual.service.ServiceException;
import javafx.collections.ObservableList;

/**
 * Created by alija on 31.03.18
 */
public interface BookingDAO {

    /**
     * Inserting new booking in database and also booked Vehicles from this Booking
     * in relation table
     * @param booking object that we want to create and put in DB
     * @throws DAOException gives database exception:
     * if some of booked vehicles is already booked for this desired period
     * and other sql exceptions
     */
    void create(Booking booking) throws DAOException;

    /**
     * List all bookings at the begin of program or later when required
     * @return Observable list of all bookings from DB
     * @throws DAOException gives database exception
     */
    ObservableList<Booking> findAll() throws DAOException;

    /**
     * Call when booking needs to be billed/canceled with charge
     * @param b Booking that we want to bill
     * @throws DAOException thrown when Booking cannot be billed
     */
    void billBooking(Booking b) throws DAOException;

    /**
     * Called when booking needs to be canceled/deleted
     * @param booking booking that we want to cancellation
     * @throws DAOException thrown when  cancellation can not be preformed in database
     */
    void cancelBooking(Booking booking) throws DAOException;

    /**
     * Called as part of update process booking, when booking needs to be updated
     * in database
     * @param b booking that we want to update in database
     * @throws DAOException thrown when booking could not be updated, because of possible overlapping
     * with already created bookings
     */
    void update(Booking b) throws DAOException;
}
