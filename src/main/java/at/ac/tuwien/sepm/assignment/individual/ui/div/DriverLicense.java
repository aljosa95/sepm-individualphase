package at.ac.tuwien.sepm.assignment.individual.ui.div;

/**
 * Created by alija on 23.03.18
 */
public enum DriverLicense {
    NOLICENSE,
    A,
    B,
    C,
    AB,
    AC,
    BC,
    ABC
}
