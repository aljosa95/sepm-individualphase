package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.dao.DAOException;
import at.ac.tuwien.sepm.assignment.individual.dao.VehicleDAO;
import at.ac.tuwien.sepm.assignment.individual.dao.impl.VehicleDAOJDBC;
import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import at.ac.tuwien.sepm.assignment.individual.service.ServiceException;
import at.ac.tuwien.sepm.assignment.individual.service.VehicleService;
import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.Calendar;

/**
 * Created by alija on 19.03.18
 */
public class VehicleServiceImpl implements VehicleService{

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private VehicleDAO vehicleDAO = new VehicleDAOJDBC();

    @Override
    public void saveVehicle(Vehicle v) throws ServiceException {
        LOG.info("Validating Vehicle {}", v);
        try{
            validateVehicle(v);
            vehicleDAO.create(v);
            LOG.info("Saved Vehicle {}", v);
        } catch (DAOException e) {
            LOG.error("Saving Vehicle Error");
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public ObservableList<Vehicle> findAll() throws ServiceException {
        try {
            return vehicleDAO.findAll();
        } catch (DAOException e) {
            LOG.error("Loading all vehicles error");
            throw new ServiceException("Could not load all vehicles from DB!");
        }
    }

    @Override
    public void updateVehicle(Vehicle v) throws ServiceException {
        try {
            validateVehicle(v);
            vehicleDAO.update(v);
        } catch (DAOException e) {
            LOG.error("Updating Vehicle Error");
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteVehicle(Vehicle v) throws ServiceException {
        try {
            if (v.isDeleted()){
                throw new ServiceException("Vehicle already deleted");
            }
            else {
                vehicleDAO.delete(v);
            }
        } catch (DAOException e) {
            LOG.error("Deleting Vehicle Error");
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteAllVehicles(ObservableList<Vehicle> vehicles) throws ServiceException {
        try {
            for (Vehicle v: vehicles) {
                if (v.isDeleted()) {
                    throw new ServiceException("Vehicle already deleted");
                } else {
                    vehicleDAO.delete(v);
                }
            }
            LOG.info("All selected vehicles deleted!");
        } catch (DAOException e) {
            LOG.error("Deleting Vehicle Error");
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public ObservableList<Vehicle> searchVehicles(Vehicle vehicle) throws ServiceException {
        ObservableList<Vehicle> vehicles = FXCollections.observableArrayList();
        try {
            if (vehicle.getBeginDate() != null) {
                if (vehicle.getBeginDate() != null && vehicle.getBeginDate().isAfter(vehicle.getEndDate())) {
                    throw new ServiceException("Begin Date cannot be after End Date!");
                }
                if (vehicle.isTimeFilter()) {
                    LocalTime begintime = LocalTime.parse(vehicle.getBeginTime());
                    LocalTime endtime = LocalTime.parse(vehicle.getEndTime());
                    if (vehicle.getBeginDate() != null && vehicle.getBeginDate().equals(vehicle.getEndDate()) && !(begintime.isBefore(endtime))) {
                        throw new ServiceException("Begin Time have to be before End Time!");
                    }
                }
            }
            if (vehicle.getSeats()!= null && vehicle.getSeats()<0){
                throw new ServiceException("Number fields cannot be less than zero!");
            }
            if (vehicle.getMaxPrice()!= null && vehicle.getMaxPrice()<0){
                throw new ServiceException("Number fields cannot be less than zero!");
            }
            if (vehicle.getMinPrice()!= null && vehicle.getMinPrice()<0){
                throw new ServiceException("Number fields cannot be less than zero!");
            }
            vehicles = vehicleDAO.searchVehicles(vehicle);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return vehicles;
    }


    private void validateVehicle(Vehicle v) throws ServiceException {
        if (v.getName().length() > 100){
            throw new ServiceException("Name too long!");
        }
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        if (v.getBuildYear() > currentYear || v.getBuildYear() < 1500){
            throw new ServiceException("Build year must be between 1500 and " + currentYear);
        }
        if (v.getSeats() != null && (v.getSeats()<0 || v.getSeats() > 1000 )){
            throw new ServiceException("Seats size must be between 0 and 1000!");
        }
        if (v.getTagNumber() != null && (v.getTagNumber().length() < 5 || v.getTagNumber().length() > 20)){
            throw new ServiceException("Tag number size must be between 5 and 20!");
        }
        if (v.getPricePerHour() < 0 || v.getPricePerHour() > 1000000){
            throw new ServiceException("Price per hour must be between 0 and 1Mil!");
        }
        if (!v.getTypeOfDrive().equals("motorized") && !v.getVehicleClass().equals(DriverLicense.NOLICENSE)){
            throw new ServiceException("Muscle Power Vehicles do not need Driving License!");
        }
        if (v.getTypeOfDrive().equals("motorized") && v.getPower() != null && (v.getPower() < 1 || v.getPower() > 10000)){
            throw new ServiceException("Power must be between 1 and 10000!");
        }
        if (v.getImg() != null && !checkImg(v)){
            throw new ServiceException("Image size must be min. 500x500 and max. 5Mb!");
        }
        if (v.getPower()!=null &&  v.getTypeOfDrive().equals("motorized") && v.getPower() > 5 && v.getVehicleClass().equals(DriverLicense.NOLICENSE)){
            throw new ServiceException("Motor vehicle with power more then 5kW needs license!");
        }
        if (v.getPower() == null && !v.getVehicleClass().equals(DriverLicense.NOLICENSE)){
            throw new ServiceException("Muscle power vehicle needs no license");
        }
    }


    /**
     * Check image properties and also save image in resources
     * cause we do not want to read image twice, reason -> performance
     * @param vehicle vehicle that we want to save a picture form
     * @return true if img has been validate successfully
     */
    private boolean checkImg(Vehicle vehicle) {
        int width, height;
        boolean goodImg = true;
        File imgFile = new File(vehicle.getImg());
        try {
            BufferedImage bufferedImage = ImageIO.read(imgFile);
            Path p = Paths.get(vehicle.getImg());
            String path = p.getFileName().toString();
            path = "/home/alija/SEPM/imgs/" + path;
            File file = new File(path);
            vehicle.setImg(path);
            width = bufferedImage.getWidth();
            height = bufferedImage.getHeight();
            if (width < 500 || height < 500 || ((imgFile.length() / (1024.0 * 1024.0)) > 5)){
                goodImg = false;
            }
            if (goodImg) {
                ImageIO.write(bufferedImage, "jpg", file);
            }
        } catch (IOException e) {
            LOG.error("Image could not been load from path");
            e.printStackTrace();
        }
        return goodImg;
    }


}
