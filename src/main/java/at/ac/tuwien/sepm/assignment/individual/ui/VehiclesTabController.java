package at.ac.tuwien.sepm.assignment.individual.ui;

import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import at.ac.tuwien.sepm.assignment.individual.service.BookingService;
import at.ac.tuwien.sepm.assignment.individual.service.ServiceException;
import at.ac.tuwien.sepm.assignment.individual.service.VehicleService;
import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;
import at.ac.tuwien.sepm.assignment.individual.ui.div.SpinnerValue;
import at.ac.tuwien.sepm.assignment.individual.ui.div.UserAlert;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by alija on 29.03.18
 */
public class VehiclesTabController  {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private UserAlert userAlert = new UserAlert();
    @FXML
    private TextField nameSearch;
    @FXML
    private TextField seatsSearch;
    @FXML
    private TextField minPriceSearch;
    @FXML
    private TextField maxPriceSearch;
    @FXML
    private DatePicker bookBeginSearch;
    @FXML
    private DatePicker bookEndSearch;
    @FXML
    private ComboBox<String> driveBox;
    @FXML
    private ComboBox<DriverLicense> classBox;

    @FXML
    private AnchorPane vehiclesTab;

    @FXML
    private ImageView imgView;

    @FXML
    private TableView<Vehicle> vehicleTable;
    @FXML
    private TableColumn<Vehicle,String> vnameColumn;
    @FXML
    private TableColumn<Vehicle,DriverLicense> vclassColumn;
    @FXML
    private TableColumn<Vehicle,String> vtypeColumn;
    @FXML
    private TableColumn<Vehicle,Long> vpriceColumn;
    @FXML
    private CheckBox bookedCheckbox,timeCheckbox;
    @FXML
    private Spinner<LocalTime> begin_spinner;
    @FXML
    private Spinner<LocalTime> end_spinner;
    @FXML
    private Group timeGroup;


    private VehicleService vehicleService;
    private BookingService bookingService;
    private Stage primaryStage;
    private ObservableList<Vehicle> vehiclesList = FXCollections.observableArrayList();
    private BookingsTabController bookingsTabController;

    @FXML
    public void initialize(){

        vehicleTable.setEditable(true);
        vehicleTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        vnameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        vclassColumn.setCellValueFactory(new PropertyValueFactory<>("vehicleClass"));
        vpriceColumn.setCellValueFactory(new PropertyValueFactory<>("pricePerHour"));
        vtypeColumn.setCellValueFactory(new PropertyValueFactory<>("typeOfDrive"));

        vehicleTable.setOnMouseClicked( event -> {
            if( event.getClickCount() == 2 ) {
                try {
                    detailsClicked();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }});

        vehicleTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Vehicle>() {
            public void changed(ObservableValue<? extends Vehicle> observable, Vehicle oldValue, Vehicle newValue) {
                if (newValue==null || newValue.getImg() == null) {
                    imgView.setVisible(false);
                } else {
                    try {
                        File file = new File(newValue.getImg());
                        Image img = new Image(file.toURI().toURL().toExternalForm());
                        imgView.setPreserveRatio(true);
                        imgView.setImage(img);
                        imgView.setVisible(true);
                    } catch (MalformedURLException e) {
                        imgView.setVisible(false);
                        LOG.error("Image ");
                    }
                }
            }
        });
        ObservableList<String> driveList = FXCollections.observableArrayList("motorized","muscle-powered");
        driveBox.setItems(driveList);
        List<DriverLicense> driverLicenses = Arrays.asList(DriverLicense.values());
        classBox.setItems(FXCollections.observableList(driverLicenses));
        begin_spinner.setValueFactory(new SpinnerValue().getStartSpinner());
        end_spinner.setValueFactory(new SpinnerValue().getStartSpinner());

    }

    public void fillVehicleTable(){
        LOG.info("Loading all Vehicles!");
        try {
            vehiclesList = vehicleService.findAll();
        } catch (ServiceException e) {
            LOG.error(e.getMessage());
        }
        vehicleTable.setItems(vehiclesList);
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }


    public void searchClicked(ActionEvent actionEvent) {
        String name = null;
        DriverLicense driverLicense = null;
        Double minPrice = null;
        Double maxPrice = null;
        boolean bookedVehicle = false;
        LocalDate beginDate = null;
        LocalDate endDate = null;
        String beginTime = null;
        String endTime = null;
        String typeOfDrive = null;
        Integer seats = null;
        boolean timeFilter = false;
        if (!nameSearch.getText().equals("")){
            name = nameSearch.getText();
        }
        if (!seatsSearch.getText().equals("")){
            try {
                seats= Integer.parseInt(seatsSearch.getText());
            }catch (NumberFormatException e){
                userAlert.showWarning("Values of number fields must be a number!");
                LOG.error("Invalid input in text field(s)");
                return;
            }
        }
        if (!minPriceSearch.getText().equals("") || !maxPriceSearch.getText().equals("")){
            try {
                if (!minPriceSearch.getText().equals("")) {
                    minPrice = Double.parseDouble(minPriceSearch.getText());
                }
                if (!maxPriceSearch.getText().equals("")){
                    maxPrice = Double.parseDouble(maxPriceSearch.getText());
                }
            }catch (NumberFormatException e){
                userAlert.showWarning("Values of number fields must be a number!");
                LOG.error("Invalid input in text field(s)");
                return;
            }
        }
        if (driveBox.getValue() != null){
            typeOfDrive = driveBox.getValue();
        }
        if (classBox.getValue() != null){
            driverLicense = classBox.getValue();
        }
        if (bookBeginSearch.getValue()== null && bookEndSearch.getValue() != null){
            userAlert.showWarning("You have to select both, begin and end date!");
            LOG.error("Only one date for search selected!");
            return;
        }
        if (bookBeginSearch.getValue()!= null && bookEndSearch.getValue() == null){
            userAlert.showWarning("You have to select both, begin and end date!");
            LOG.error("Only one date for search selected!");
            return;
        }
        if (bookBeginSearch.getValue() != null){
            beginDate = bookBeginSearch.getValue();
        }
        if (bookEndSearch.getValue() != null){
            endDate = bookEndSearch.getValue();
        }
        if (timeGroup.isVisible() && bookBeginSearch.getValue()!= null && bookEndSearch.getValue() != null){
            beginTime = begin_spinner.getValue().toString();
            endTime = end_spinner.getValue().toString();
            timeFilter = true;

        }
        if ((timeGroup.isVisible()) && bookBeginSearch.getValue()== null && bookEndSearch.getValue() == null){
            userAlert.showWarning("You have to select both, begin and end date!");
            LOG.error("Only time but not date for search selected!");
            return;
        }
        if (bookedCheckbox.isSelected()){
            bookedVehicle = true;
        }
        Vehicle vehicle = new Vehicle(name,seats,typeOfDrive,driverLicense,minPrice,maxPrice,bookedVehicle,beginDate,endDate,beginTime,endTime, timeFilter);

        try {
            vehicleTable.setItems(vehicleService.searchVehicles(vehicle));
            LOG.info("Searching for Vehicles..");
        } catch (ServiceException e) {
            LOG.error(e.getClass().getSimpleName() + " " + e.getMessage());
            userAlert.showWarning(e.getMessage());
        }


    }

    public void refreshClicked(ActionEvent actionEvent) {
        seatsSearch.clear();
        nameSearch.clear();
        maxPriceSearch.clear();
        minPriceSearch.clear();
        bookBeginSearch.setValue(null);
        bookEndSearch.setValue(null);
        bookedCheckbox.setSelected(false);
        driveBox.setValue(null);
        classBox.setValue(null);
        fillVehicleTable();
        LOG.info("Values restored to default in search bar!");
    }

    public void deleteClicked(ActionEvent actionEvent) {
        if (vehicleTable.getSelectionModel().getSelectedItem() == null){
            userAlert.showInfo("Please select one or more vehicle(s)!");
        }
        else {
            ObservableList<Vehicle> selectedItems = vehicleTable.getSelectionModel().getSelectedItems();
            StringBuilder vehicles = new StringBuilder();
            Iterator<Vehicle> iterator = selectedItems.iterator();
            vehicles.append(iterator.next().getName());
            while (iterator.hasNext()) {
                vehicles.append(", ");
                vehicles.append(iterator.next().getName());
            }
            if (userAlert.showConfirmation("Are you sure that you want to delete vehicle(s): " + vehicles)) {
                try {
                    vehicleService.deleteAllVehicles(selectedItems);
                    fillVehicleTable();
                } catch (ServiceException e) {
                    LOG.error(e.getClass().getSimpleName() + " " + e.getMessage());
                    userAlert.showWarning(e.getMessage());
                }
            }
        }
    }

    public void detailsClicked() throws  IOException {
        if (vehicleTable.getSelectionModel().getSelectedItems().size() > 1){
            userAlert.showInfo("Please select one vehicle!");
        }
        else if (vehicleTable.getSelectionModel().getSelectedItem() == null){
            userAlert.showInfo("Please select one vehicle!");
        }
        else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/detailsEdit.fxml"));
            Scene scene = new Scene(loader.load());
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(scene);

            CreateVehicleController createVehicleController = loader.getController();
            createVehicleController.setCreationMode(false);
            createVehicleController.setStage(stage);
            createVehicleController.setVehicleService(vehicleService);
            createVehicleController.setBookingService(bookingService);
            createVehicleController.setBookingsTabController(bookingsTabController);
            createVehicleController.setVehiclesTabController(this);
            Vehicle v = vehicleTable.getSelectionModel().getSelectedItem();
            for (Vehicle v1 :vehiclesList) {
                if (v.getId().equals(v1.getId())){
                    v = v1;
                    break;
                }
            }
            createVehicleController.setVehicle(v);
            createVehicleController.loadData();
            stage.showAndWait();
            LOG.info("Details clicked..");
        }
    }

    public void addToBookingClicked(ActionEvent actionEvent)  {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/createBooking.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(loader.load());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        stage.setScene(scene);

        CreateBookingController createBookingController = loader.getController();
        createBookingController.setCreationMode(true);
        createBookingController.setStage(stage);
        createBookingController.setVehicleService(vehicleService);
        createBookingController.setBookingService(bookingService);
        createBookingController.setBookingsTabController(bookingsTabController);
        createBookingController.getVehicleTable().setItems(vehicleTable.getSelectionModel().getSelectedItems());
        stage.showAndWait();
    }

    public void setVehicleService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @FXML
    private void timeCheckboxClicked(ActionEvent actionEvent){
        if (timeGroup.isVisible()){
            timeGroup.setVisible(false);
        }
        else timeGroup.setVisible(true);
    }
    public ObservableList<Vehicle> getVehiclesList() {
        return vehiclesList;
    }

    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public BookingsTabController getBookingsTabController() {
        return bookingsTabController;
    }

    public void setBookingsTabController(BookingsTabController bookingsTabController) {
        this.bookingsTabController = bookingsTabController;
    }
}
