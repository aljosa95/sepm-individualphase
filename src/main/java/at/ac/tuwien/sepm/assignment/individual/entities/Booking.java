package at.ac.tuwien.sepm.assignment.individual.entities;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


import java.time.LocalDate;
import java.time.LocalDateTime;

public class Booking {

    private Integer id;
    private String customerName;
    private String cardNumber;
    private LocalDate dateBegin;
    private LocalDate dateEnd;
    private String timeBegin;
    private String timeEnd;
    private LocalDateTime createTimestamp;
    private LocalDateTime billTimestamp;
    private String status;
    private Double totalPrice;
    private Integer billNumber;
    private ObservableList<BookedVehicle> bookedVehicles = FXCollections.observableArrayList();

    public Booking(String customerName, String cardNumber, LocalDate bookingBegin, LocalDate bookingEnd) {
        this.customerName = customerName;
        this.cardNumber = cardNumber;
        this.dateBegin = bookingBegin;
        this.dateEnd = bookingEnd;
    }


    public Booking(String customerName, String cardNumber, LocalDate dateBegin, LocalDate dateEnd, String timeBegin, String timeEnd) {
        this.customerName = customerName;
        this.cardNumber = cardNumber;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.timeBegin = timeBegin;
        this.timeEnd = timeEnd;
    }

    public Booking() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public LocalDate getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(LocalDate dateBegin) {
        this.dateBegin = dateBegin;
    }

    public LocalDate getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }




    public String getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(String timeBegin) {
        this.timeBegin = timeBegin;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public ObservableList<BookedVehicle> getBookedVehicles() {
        return bookedVehicles;
    }

    public void setBookedVehicles(ObservableList<BookedVehicle> bookedVehicles) {
        this.bookedVehicles = bookedVehicles;
    }




    public Integer getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(Integer billNumber) {
        this.billNumber = billNumber;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDateTime getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(LocalDateTime createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public LocalDateTime getBillTimestamp() {
        return billTimestamp;
    }

    public void setBillTimestamp(LocalDateTime billTimestamp) {
        this.billTimestamp = billTimestamp;
    }

    @Override
    public String toString() {
        return "Booking{" +
            "customerName='" + customerName + '\'' +
            ", cardNumber='" + cardNumber + '\'' +
            ", dateBegin=" + dateBegin +
            ", dateEnd=" + dateEnd +
            ", timeBegin='" + timeBegin + '\'' +
            ", timeEnd='" + timeEnd + '\'' +
            ", createTimestamp=" + createTimestamp +
            ", billTimestamp=" + billTimestamp +
            ", status='" + status + '\'' +
            ", totalPrice=" + totalPrice +
            ", billNumber=" + billNumber +
            '}';
    }
}
