package at.ac.tuwien.sepm.assignment.individual.entities;

import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by Alija on 18.03.2018
 */
public class Vehicle {

    private Integer id;
    private String name;
    private Integer buildYear;
    private String description;
    private Integer seats;
    private String tagNumber;
    private String typeOfDrive;
    private Long power;
    private Double pricePerHour;
    private LocalDate createdDate;
    private LocalTime createTime;
    private LocalDate editDate;
    private LocalTime editTime;
    private DriverLicense vehicleClass;
    private String img;
    private boolean isDeleted;

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public boolean isBookedVehicle() {
        return bookedVehicle;
    }

    public void setBookedVehicle(boolean bookedVehicle) {
        this.bookedVehicle = bookedVehicle;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    //for search
    private Double minPrice;
    private Double maxPrice;
    private boolean bookedVehicle;
    private LocalDate beginDate;
    private LocalDate endDate;
    private String beginTime;
    private String endTime;
    private boolean timeFilter;

    public Vehicle(String name, Integer seats, String typeOfDrive, DriverLicense vehicleClass, Double minPrice, Double maxPrice, boolean bookedVehicle, LocalDate beginDate, LocalDate endDate, String beginTime, String endTime, boolean timeFilter) {
        this.name = name;
        this.seats = seats;
        this.typeOfDrive = typeOfDrive;
        this.vehicleClass = vehicleClass;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.bookedVehicle = bookedVehicle;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.timeFilter = timeFilter;
    }

    public Vehicle() {
    }

    public Vehicle(String name, Integer buildYear, String typeOfDrive, Double pricePerHour) {
        this.name = name;
        this.buildYear = buildYear;
        this.typeOfDrive = typeOfDrive;
        this.pricePerHour = pricePerHour;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBuildYear() {
        return buildYear;
    }

    public void setBuildYear(Integer buildYear) {
        this.buildYear = buildYear;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public String getTagNumber() {
        return tagNumber;
    }

    public void setTagNumber(String tagNumber) {
        this.tagNumber = tagNumber;
    }

    public String getTypeOfDrive() {
        return typeOfDrive;
    }

    public void setTypeOfDrive(String typeOfDrive) {
        this.typeOfDrive = typeOfDrive;
    }

    public Long getPower() {
        return power;
    }

    public void setPower(Long power) {
        this.power = power;
    }

    public Double getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(Double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public DriverLicense getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(DriverLicense vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public LocalDate getEditDate() {
        return editDate;
    }

    public void setEditDate(LocalDate editDate) {
        this.editDate = editDate;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
            "name='" + name + '\'' +
            ", buildYear=" + buildYear +
            ", description='" + description + '\'' +
            ", seats=" + seats +
            ", tagNumber='" + tagNumber + '\'' +
            ", typeOfDrive='" + typeOfDrive + '\'' +
            ", power=" + power +
            ", pricePerHour=" + pricePerHour +
            ", createdDate=" + createdDate +
            ", editDate=" + editDate +
            ", vehicleClass=" + vehicleClass +
            ", img='" + img + '\'' +
            ", isDeleted=" + isDeleted +
            '}';
    }

    public LocalTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalTime createTime) {
        this.createTime = createTime;
    }

    public LocalTime getEditTime() {
        return editTime;
    }

    public void setEditTime(LocalTime editTime) {
        this.editTime = editTime;
    }

    public boolean isTimeFilter() {
        return timeFilter;
    }

    public void setTimeFilter(boolean timeFilter) {
        this.timeFilter = timeFilter;
    }
}
