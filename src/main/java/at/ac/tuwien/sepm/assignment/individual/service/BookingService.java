package at.ac.tuwien.sepm.assignment.individual.service;

import at.ac.tuwien.sepm.assignment.individual.entities.Booking;
import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import javafx.collections.ObservableList;

/**
 * Created by alija on 31.03.18
 */
public interface BookingService {

    /**
     * Add new booking in database
     * @param b object of booking that we want to save
     * @throws ServiceException return Exception for possible Validation errors:
     * - if customerName is too long,
     * - if begin Time/Date is after end Time/Date,
     * - if credit card or iban cannot be validated
     * - if license data(number and date) are not valid
     */
    void saveBooking(Booking b) throws ServiceException;

    /**
     * List all bookings at the begin of program or later when it is required
     * @return Observable list of all bookings from DB
     * @throws ServiceException exception thrown for DB failures during loading
     */
    ObservableList<Booking> findAll() throws ServiceException;

    /**
     * Called when booking needs to be billed/canceled with charge
     * @param b Booking that we want to bill
     * @throws ServiceException thrown when time of billing is not valid
     * - if booking tries to be billed before booking time
     */
    void billBooking(Booking b) throws ServiceException;

    /**
     * Called when booking needs to be canceled/deleted
     * @param booking booking that we want to cancellation
     * @throws ServiceException thrown when time of cancellation is not valid
     * - if booking tries to be canceled after booking time
     */
    void cancelBooking(Booking booking) throws ServiceException;

    /**
     * Called when we have to update booking and its values
     * @param booking booking that we want to update
     * @throws ServiceException thrown when booking could not be updated, because of
     * possible different validation errors:
     * - same input validation as described @saveBooking
     * - if booking tries to be updated less than one week before booking
     */
    void updateBooking(Booking booking) throws ServiceException;
}
