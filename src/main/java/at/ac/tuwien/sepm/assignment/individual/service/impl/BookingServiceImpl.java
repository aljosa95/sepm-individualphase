package at.ac.tuwien.sepm.assignment.individual.service.impl;

import at.ac.tuwien.sepm.assignment.individual.dao.BookingDAO;
import at.ac.tuwien.sepm.assignment.individual.dao.DAOException;
import at.ac.tuwien.sepm.assignment.individual.dao.VehicleDAO;
import at.ac.tuwien.sepm.assignment.individual.dao.impl.BookingDAOJDBC;
import at.ac.tuwien.sepm.assignment.individual.dao.impl.VehicleDAOJDBC;
import at.ac.tuwien.sepm.assignment.individual.entities.BookedVehicle;
import at.ac.tuwien.sepm.assignment.individual.entities.Booking;
import at.ac.tuwien.sepm.assignment.individual.service.BookingService;
import at.ac.tuwien.sepm.assignment.individual.service.ServiceException;
import javafx.collections.ObservableList;
import org.apache.commons.validator.routines.CreditCardValidator;
import org.apache.commons.validator.routines.IBANValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

/**
 * Created by alija on 31.03.18
 */
public class BookingServiceImpl implements BookingService {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private BookingDAO bookingDAO = new BookingDAOJDBC();


    @Override
    public void saveBooking(Booking b) throws ServiceException {
        validateBooking(b);
        try {
            LOG.info("Saving Booking");
            bookingDAO.create(b);
            LOG.info("Saved Booking " + b);
        } catch (DAOException e) {
            LOG.error("Saving Booking Error!");
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public ObservableList<Booking> findAll() throws ServiceException {
        try {
            ObservableList<Booking> bookings = bookingDAO.findAll();
            LocalDate dateBegin;
            LocalDate dateEnd;
            for (Booking booking: bookings) {
                int numOfHours;
                dateBegin = booking.getDateBegin();
                dateEnd = booking.getDateEnd();
                int days = Period.between(dateBegin,dateEnd).getDays();
                Integer beginTime = Integer.parseInt(booking.getTimeBegin().substring(0,2));
                Integer endTime = Integer.parseInt(booking.getTimeEnd().substring(0,2));
                if (days==0){
                    numOfHours = endTime-beginTime;
                }
                else {
                    int hourdiff = endTime-beginTime;
                    if (hourdiff<0){
                        days--;
                        numOfHours = days*24;
                        numOfHours += endTime;
                        numOfHours += 24-beginTime;
                    }
                    else if (hourdiff==0){
                        numOfHours = days*24;
                    }
                    else {
                        numOfHours = endTime-beginTime;
                        numOfHours += days * 24;
                    }
                }
                double ppHofAllVehicles = 0;
                for (BookedVehicle bv: booking.getBookedVehicles()) {
                    ppHofAllVehicles += bv.getPricePerHour();
                }
                booking.setTotalPrice(ppHofAllVehicles*numOfHours);
                if (booking.getStatus().equals("canceled")){
                    calculateCharge(booking);
                }
            }
            return bookings;
        } catch (DAOException e) {
            LOG.error("Err by loading bookings!");
            throw new ServiceException("Could not load Bookings from Database!");
        }

    }

    private void calculateCharge(Booking booking) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDate cancelDate =  booking.getBillTimestamp().toLocalDate();
        String  billHourStr = booking.getBillTimestamp().toLocalTime().toString().substring(0,2);
        Integer cancelHour = Integer.parseInt(billHourStr);
        Integer beginTime = Integer.parseInt(booking.getTimeBegin().substring(0,2));
        LocalDate beginDate = booking.getDateBegin();
        int days = Period.between(cancelDate,beginDate).getDays();
        int hours = beginTime-cancelHour;
        double charge;
        if (days>3 || (days==3 && hours>0)){
            charge = 0.75;
        }
        else if (days==2 || (days==1 && hours>0)){
            charge = 0.40;
        }
        else {
            charge = 1;
        }
        booking.setTotalPrice(booking.getTotalPrice()*charge);

    }

    @Override
    public void billBooking(Booking booking) throws ServiceException {
        try {
            LocalDate dateNow = LocalDate.now();
            Integer hourNow = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            Integer bookTime = Integer.parseInt(booking.getTimeBegin().substring(0,2));
            if (dateNow.isAfter(booking.getDateBegin()) || (dateNow.isEqual(booking.getDateBegin()) && (hourNow-bookTime)>0)) {
                booking.setStatus("billed");
                bookingDAO.billBooking(booking);
            }
            else throw new ServiceException("Booking can be billed only after booking begin time!");
        } catch (DAOException e) {
            LOG.error("Billing booking error: " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void cancelBooking(Booking booking) throws ServiceException {
        try {
            boolean deleteBooking = false;
            LocalDate dateNow = LocalDate.now();
            Integer hourNow = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            Integer beginTime = Integer.parseInt(booking.getTimeBegin().substring(0,2));
            LocalDate beginDate = booking.getDateBegin();
            int days = Period.between(dateNow,beginDate).getDays();
            int months = Period.between(dateNow,beginDate).getMonths();
            int years = Period.between(dateNow,beginDate).getYears();
            if (days>7 || months>=1 || years>0){
                deleteBooking = true;
            }
            else if (days==7 && (beginTime-hourNow)>0){
                deleteBooking = true;
            }
            else if (days==7 && (beginTime-hourNow)<=0){
                deleteBooking = false;
            }
            else if (days>0){
                deleteBooking = false;
            }
            else if (days == 0 && (beginTime-hourNow)>0){
                deleteBooking = false;
            }
            else throw new ServiceException("Booking can be canceled only before booking begin time!");
            if (deleteBooking){
                booking.setStatus("canceled");
                bookingDAO.cancelBooking(booking);
            }
            else{
                booking.setStatus("canceled");
                bookingDAO.billBooking(booking);
            }
        } catch (DAOException e) {
            LOG.error("Cancelling booking error: " + e.getMessage());
            throw new ServiceException(e.getMessage());
        }

    }

    @Override
    public void updateBooking(Booking b) throws ServiceException {
        validateBooking(b);
        try {
            LOG.info("Updating Booking");
            LocalDate dateBegin = b.getDateBegin();
            LocalDate dateNow = LocalDate.now();
            Integer days = Period.between(dateNow,dateBegin).getDays();
            if (days<=7){
                throw new ServiceException("Booking can be updated only one week before booking time!");
            }
            bookingDAO.update(b);
            LOG.info("Updated Booking " + b);
        } catch (DAOException e) {
            LOG.error("Update Booking Error!");
            throw new ServiceException(e.getMessage());
        }
    }


    private void validateBooking(Booking b) throws ServiceException {

        if (b.getCustomerName().length() > 100){
            throw new ServiceException("Name is too long, maximum length: 100!");
        }

        LocalDate dateBegin = b.getDateBegin();
        LocalDate dateEnd = b.getDateEnd();
        LocalDate dateNow = LocalDate.now();
        if (dateBegin.isBefore(dateNow) || dateEnd.isBefore(dateBegin)){
            throw new ServiceException("Date values cannot be before actual date and also end date cannot be before begin date!");
        }

        Integer hourNow = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        Integer bookTime = Integer.parseInt(b.getTimeBegin().substring(0,2));
        Integer endTime = Integer.parseInt(b.getTimeEnd().substring(0,2));
        if (dateBegin.equals(dateNow) && ((hourNow-bookTime)>0)){
            throw new ServiceException("Booking begin time cannot be before actual time if the date is the same!");
        }
        if (dateBegin.equals(dateEnd) && ((bookTime-endTime)>=0)){
            throw new ServiceException("Booking end time cannot be before begin time if the date is the same!");
        }


        CreditCardValidator creditCardValidator = new CreditCardValidator();
        if (!IBANValidator.getInstance().isValid(b.getCardNumber()) && !creditCardValidator.isValid(b.getCardNumber())){
            throw new ServiceException("Your IBAN/Cardnumber ist not valid!");
        }
        if (!b.getBookedVehicles().isEmpty()){
            for (BookedVehicle bvehicle : b.getBookedVehicles()) {
                if (bvehicle.getLicenseNumber()!=null && bvehicle.getLicenseNumber().length()>20){
                    throw new ServiceException("License Number for Vehicle: " + bvehicle.getName() + " is not valid!");
                }
                if (bvehicle.getLicenseDate()!=null && bvehicle.getLicenseDate().isAfter(LocalDate.now())){
                    throw new ServiceException("License Date of Issue for Vehicle: " + bvehicle.getName() + " is not valid!");
                }
            }
        }
    }


}
