package at.ac.tuwien.sepm.assignment.individual.application;

import at.ac.tuwien.sepm.assignment.individual.service.BookingService;
import at.ac.tuwien.sepm.assignment.individual.service.VehicleService;
import at.ac.tuwien.sepm.assignment.individual.service.impl.BookingServiceImpl;
import at.ac.tuwien.sepm.assignment.individual.service.impl.VehicleServiceImpl;
import at.ac.tuwien.sepm.assignment.individual.ui.MainController;
import at.ac.tuwien.sepm.assignment.individual.ui.VehiclesTabController;
import at.ac.tuwien.sepm.assignment.individual.util.DBUtil;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public final class MainApplication extends Application {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void start(Stage primaryStage) throws Exception {
        // setup application
        primaryStage.setTitle("Car Rental");

        primaryStage.centerOnScreen();
        VehicleService vehicleService = new VehicleServiceImpl();
        BookingService bookingService = new BookingServiceImpl();

        // prepare fxml loader to inject controller
        FXMLLoader fxmlMainLoader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        Parent node = fxmlMainLoader.load();

        // initiate controller
        MainController mainController = fxmlMainLoader.getController();
        mainController.setVehicleService(vehicleService);
        mainController.setBookingService(bookingService);
        mainController.setPrimaryStage(primaryStage);
        mainController.loadData();
        primaryStage.setScene(new Scene(node));

        // show application
        primaryStage.show();
        primaryStage.toFront();
        LOG.debug("Application startup complete");
        primaryStage.setOnCloseRequest(event -> DBUtil.closeConnection());
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                DBUtil.closeConnection();
                LOG.debug("Application shutdown initiated");
            }
        });

    }


    public static void main(String[] args) {
        LOG.debug("Application starting with arguments={}", (Object) args);
        Application.launch(MainApplication.class, args);
    }

}
