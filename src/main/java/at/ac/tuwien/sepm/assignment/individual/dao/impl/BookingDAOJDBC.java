package at.ac.tuwien.sepm.assignment.individual.dao.impl;

import at.ac.tuwien.sepm.assignment.individual.dao.BookingDAO;
import at.ac.tuwien.sepm.assignment.individual.dao.DAOException;
import at.ac.tuwien.sepm.assignment.individual.entities.BookedVehicle;
import at.ac.tuwien.sepm.assignment.individual.entities.Booking;
import at.ac.tuwien.sepm.assignment.individual.util.DBUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Date;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.WeakHashMap;

/**
 * Created by alija on 31.03.18
 */
public class BookingDAOJDBC implements BookingDAO {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


    @Override
    public void create(Booking booking) throws DAOException {
        LOG.info("Adding booking to DB {}", booking);
        try {
            if (booking==null){
                throw new NullPointerException();
            }

            checkBooking(booking);

            String bookingInsertString = "INSERT INTO BOOKING (BID,NAME,CARD_NUMBER,BEGIN_DATE,END_DATE,BEGIN_TIME,END_TIME,CREATE_TIMESTAMP) VALUES (default,?,?,?,?,?,?,?)";
            PreparedStatement ps = DBUtil.getConnection().prepareStatement(bookingInsertString, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,booking.getCustomerName());
            ps.setString(2,booking.getCardNumber());
            ps.setDate(3, java.sql.Date.valueOf(booking.getDateBegin()));
            ps.setDate(4, java.sql.Date.valueOf(booking.getDateEnd()));
            ps.setString(5,booking.getTimeBegin());
            ps.setString(6,booking.getTimeEnd());
            ps.setTimestamp(7,Timestamp.valueOf(LocalDateTime.now()));


            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            generatedKeys.next();
            booking.setId(generatedKeys.getInt(1));
            String bvInsertString = "INSERT INTO bookedVehicle (BID,VID,LICENSE_NUMBER,LICENSE_DATE,VEHICLE_NAME,PRICE_PER_HOUR) VALUES (?,?,?,?,?,?)";
            PreparedStatement ps2 = DBUtil.getConnection().prepareStatement(bvInsertString, Statement.RETURN_GENERATED_KEYS);
            for (BookedVehicle bv : booking.getBookedVehicles()) {
                bv.setBid(booking.getId());
                ps2.setInt(1,bv.getBid());
                ps2.setInt(2,bv.getVid());
                ps2.setString(3,bv.getLicenseNumber());
                if (bv.getLicenseDate() != null) {
                    ps2.setDate(4, java.sql.Date.valueOf(bv.getLicenseDate()));
                }
                else ps2.setDate(4,null);
                ps2.setString(5, bv.getName());
                ps2.setDouble(6,bv.getPricePerHour());
                ps2.executeUpdate();
            }

            LOG.info("Added to Database {}",booking);

        } catch (NullPointerException | SQLException e) {
            LOG.error("SQL Exception at BookingDAO by adding Booking");
            throw new DAOException(e.getMessage());
        }

    }

    @Override
    public ObservableList<Booking> findAll() throws DAOException {
        ObservableList<Booking> bookings = FXCollections.observableArrayList();
        try {
            PreparedStatement ps = DBUtil.getConnection().prepareStatement("SELECT * FROM BOOKING");
            PreparedStatement ps2 = DBUtil.getConnection().prepareStatement("SELECT * FROM bookedVehicle WHERE BID = ?");
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Booking booking = new Booking();
                booking.setId(rs.getInt(1));
                booking.setCustomerName(rs.getString(2));
                booking.setCardNumber(rs.getString(3));
                booking.setDateBegin(rs.getDate(4).toLocalDate());
                booking.setDateEnd(rs.getDate(5).toLocalDate());
                booking.setTimeBegin(rs.getString(6));
                booking.setTimeEnd(rs.getString(7));
                booking.setCreateTimestamp(rs.getTimestamp(8).toLocalDateTime());
                if (rs.getTime(9)!=null) {
                    booking.setBillTimestamp(rs.getTimestamp(9).toLocalDateTime());
                    booking.setBillNumber(rs.getInt(11));
                }
                booking.setStatus(rs.getString(10));


                ps2.setInt(1,booking.getId());

                ResultSet rs2 = ps2.executeQuery();
                while (rs2.next()){
                    BookedVehicle bookedVehicle = new BookedVehicle();

                    bookedVehicle.setBid(rs2.getInt(1));
                    bookedVehicle.setVid(rs2.getInt(2));
                    bookedVehicle.setLicenseNumber(rs2.getString(3));
                    if (rs2.getDate(4) != null) {
                        bookedVehicle.setLicenseDate(rs2.getDate(4).toLocalDate());
                    }
                    bookedVehicle.setName(rs2.getString(5));
                    bookedVehicle.setPricePerHour(rs2.getDouble(6));
                    booking.getBookedVehicles().add(bookedVehicle);


                }
                bookings.add(booking);
            }

            rs.close();
        } catch (SQLException e) {
            LOG.error("SQL Exception at BookingDAO by loading bookings");
            LOG.error(e.getMessage());
            throw new DAOException(e.getMessage());
        }
        LOG.info("Bookings loaded from DB");
        return bookings;
    }

    @Override
    public void billBooking(Booking b) throws DAOException {
        try {
            PreparedStatement ps = DBUtil.getConnection().prepareStatement("UPDATE BOOKING SET BILL_TIMESTAMP = ?,BILL_NUMBER = ? ,STATUS=? WHERE BID = ? AND STATUS='open'");
            ps.setTimestamp(1,Timestamp.valueOf(LocalDateTime.now()));
            ps.setInt(2,b.getBillNumber());
            ps.setString(3,b.getStatus());
            ps.setInt(4,b.getId());

            ps.executeUpdate();
        } catch (NullPointerException | SQLException e) {
            LOG.error("Billing booking database error: " + e.getMessage());
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public void cancelBooking(Booking booking) throws DAOException {
        try {
            PreparedStatement ps = DBUtil.getConnection().prepareStatement("DELETE FROM BOOKEDVEHICLE  WHERE BID = ?");
            PreparedStatement ps2 = DBUtil.getConnection().prepareStatement("DELETE FROM BOOKING  WHERE BID = ?");
            ps2.setInt(1,booking.getId());
            ps.setInt(1,booking.getId());
            ps.executeUpdate();
            ps2.executeUpdate();
        } catch (NullPointerException | SQLException e) {
            LOG.error("Cancellation booking database error: " + e.getMessage());
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public void update(Booking booking) throws DAOException {

        LOG.info("Updating booking to DB {}", booking);
        try {
            ObservableList<Booking> bookings = findAll();
            ObservableList<BookedVehicle> allBVs = booking.getBookedVehicles();
            Booking oldBooking = booking;
            for (Booking b:bookings) {
                if (b.getId().equals(booking.getId())){
                    oldBooking = b;
                }
            }
            ObservableList<BookedVehicle> vehiclesToCheck = FXCollections.observableArrayList();
            for (BookedVehicle bvNew: booking.getBookedVehicles()) {
                for (BookedVehicle bvOld: oldBooking.getBookedVehicles()) {
                    if (!bvNew.getVid().equals(bvOld.getVid())){
                        vehiclesToCheck.add(bvNew);
                    }
                }
            }
            booking.setBookedVehicles(vehiclesToCheck);
            checkBooking(booking);
            booking.setBookedVehicles(allBVs);

            String bookingUpdateString = "UPDATE BOOKING SET NAME=?,CARD_NUMBER=?,BEGIN_DATE=?,END_DATE=?,BEGIN_TIME=?,END_TIME=? WHERE BID=?";
            PreparedStatement ps = DBUtil.getConnection().prepareStatement(bookingUpdateString, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,booking.getCustomerName());
            ps.setString(2,booking.getCardNumber());
            ps.setDate(3, Date.valueOf(booking.getDateBegin()));
            ps.setDate(4, Date.valueOf(booking.getDateEnd()));
            ps.setString(5,booking.getTimeBegin());
            ps.setString(6,booking.getTimeEnd());
            ps.setInt(7,booking.getId());


            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            generatedKeys.next();
            //String bvInsertString = "INSERT INTO bookedVehicle (BID,VID,LICENSE_NUMBER,LICENSE_DATE,VEHICLE_NAME,PRICE_PER_HOUR) VALUES (?,?,?,?,?,?) ON DUPLICATE KEY UPDATE VID=1";
            String bvInsertString = "INSERT INTO bookedVehicle (BID,VID,LICENSE_NUMBER,LICENSE_DATE,VEHICLE_NAME,PRICE_PER_HOUR) VALUES (?,?,?,?,?,?)";
            PreparedStatement psInsert = DBUtil.getConnection().prepareStatement(bvInsertString, Statement.RETURN_GENERATED_KEYS);
            //PreparedStatement psUpdate = DBUtil.getConnection().prepareStatement(bvupdateString, Statement.RETURN_GENERATED_KEYS);
            //PreparedStatement psDelete = DBUtil.getConnection().prepareStatement(bvdeleteString, Statement.RETURN_GENERATED_KEYS);
            PreparedStatement psDelete = DBUtil.getConnection().prepareStatement("DELETE  FROM bookedVehicle WHERE BID=?");
            psDelete.setInt(1,booking.getId());
            psDelete.execute();
            for (BookedVehicle bv : booking.getBookedVehicles()) {
                psInsert.setInt(1,booking.getId());
                psInsert.setInt(2,bv.getVid());
                psInsert.setString(3,bv.getLicenseNumber());
                if (bv.getLicenseDate() != null) {
                    psInsert.setDate(4, Date.valueOf(bv.getLicenseDate()));
                }
                else psInsert.setDate(4,null);
                psInsert.setString(5, bv.getName());
                psInsert.setDouble(6,bv.getPricePerHour());
                psInsert.executeUpdate();
            }
            LOG.info("Updated to Database {}",booking);

        } catch (NullPointerException | SQLException e) {
            LOG.error("SQL Exception at BookingDAO by updating Booking");
            throw new DAOException(e.getMessage());
        }


    }


    private void checkBooking(Booking booking) throws DAOException{
        try {

            String checkString = "SELECT BEGIN_DATE,END_DATE,BEGIN_TIME,END_TIME FROM bookedVehicle bv JOIN BOOKING b ON b.bid=bv.bid  WHERE VID=? AND STATUS='open'";
            PreparedStatement psCheck = DBUtil.getConnection().prepareStatement(checkString, Statement.RETURN_GENERATED_KEYS);
            for (BookedVehicle bv : booking.getBookedVehicles()) {
                psCheck.setInt(1, bv.getVid());
                ResultSet rs = psCheck.executeQuery();
                while (rs.next()) {
                    LocalDate old_begin = rs.getDate(1).toLocalDate();
                    LocalDate old_end = rs.getDate(2).toLocalDate();
                    LocalDate new_begin = booking.getDateBegin();
                    LocalDate new_end = booking.getDateEnd();
                    LocalTime timeBegin_old = LocalTime.parse(rs.getString(3));
                    LocalTime timeEnd_old = LocalTime.parse(rs.getString(4));
                    LocalTime timeBegin_new = LocalTime.parse(booking.getTimeBegin());
                    LocalTime timeEnd_new = LocalTime.parse(booking.getTimeEnd());
                    if (new_begin.isAfter(old_begin) && new_begin.isBefore(old_end)) {
                        throw new DAOException(bv.getName() + " is already booked for this period!");
                    } else if (new_begin.isBefore(old_begin) && new_end.isAfter(old_end)) {
                        throw new DAOException(bv.getName() + " is already booked for this period!");

                    } else if (new_begin.isBefore(old_begin) && new_end.isAfter(old_begin) && new_end.isBefore(old_end)) {
                        throw new DAOException(bv.getName() + " is already booked for this period!");
                    } else if (new_begin.isEqual(old_begin) && (new_end.isBefore(old_end) || new_end.isAfter(old_end))) {
                        throw new DAOException(bv.getName() + " is already booked for this period!");
                    } else if (new_begin.isEqual(old_begin) && new_end.isEqual(old_end)) {
                        if (timeBegin_new.isAfter(timeBegin_old) && timeBegin_new.isBefore(timeEnd_old)) {
                            throw new DAOException(bv.getName() + " is already booked for this period!");
                        } else if (timeBegin_new.toString().equals(timeBegin_old.toString())) {
                            throw new DAOException(bv.getName() + " is already booked for this period!");
                        } else if (timeBegin_new.isBefore(timeBegin_old) && (timeEnd_new.isAfter(timeEnd_old) || timeEnd_new.isBefore(timeEnd_old) || timeEnd_new.toString().equals(timeEnd_old.toString()))) {
                            throw new DAOException(bv.getName() + " is already booked for this period!");
                        }
                    } else if (new_begin.equals(old_end) && timeBegin_new.isBefore(timeEnd_old)) {
                        throw new DAOException(bv.getName() + " 8is already booked for this period!");

                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("SQL Exception at BookingDAO by checking Booking");
            throw new DAOException(e.getMessage());
        }
    }

}
