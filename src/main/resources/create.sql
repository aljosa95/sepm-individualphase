
DROP TABLE IF EXISTS Vehicle;
DROP TABLE IF EXISTS Booking;
DROP TABLE IF EXISTS bookedVehicle;


CREATE TABLE Vehicle(
  vid  INTEGER AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  build_year INTEGER NOT NULL,
  description VARCHAR(255),
  seats INTEGER DEFAULT NULL,
  tag_number VARCHAR(30) DEFAULT NULL ,
  type_of_drive VARCHAR(30),
  CONSTRAINT type_of_drive CHECK (type_of_drive IN ('motorized', 'muscle-powered')),
  power INTEGER DEFAULT NULL,
  price_per_hour DECIMAL(10,2) NOT NULL,
  vehicle_class VARCHAR(128),
  image VARCHAR(255) DEFAULT NULL ,
  created_date DATE NOT NULL ,
  created_time TIME NOT NULL,
  edit_date DATE NOT NULL ,
  edit_time TIME NOT NULL,
  isDeleted BOOLEAN DEFAULT FALSE
);

CREATE TABLE Booking(
  bid INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  card_number VARCHAR(255) NOT NULL,
  begin_date DATE NOT NULL,
  end_date DATE NOT NULL,
  begin_time VARCHAR(128) NOT NULL ,
  end_time VARCHAR(128) NOT NULL,
  create_timestamp SMALLDATETIME NOT NULL,
  bill_timestamp SMALLDATETIME DEFAULT NULL,
  status VARCHAR(128) DEFAULT 'open' CHECK (status IN ('open', 'billed', 'canceled')),
  bill_number INTEGER DEFAULT NULL
);



CREATE TABLE bookedVehicle(
  bid INTEGER REFERENCES  Booking(bid),
  vid INTEGER REFERENCES  Vehicle(vid),
  license_number VARCHAR(128),
  license_date DATE ,
  vehicle_name VARCHAR(128) NOT NULL,
  price_per_hour DECIMAL(10,2) NOT NULL,
);




INSERT INTO Vehicle (name,build_year,description,seats,tag_number,type_of_drive,power,price_per_hour,image,vehicle_class,created_date,edit_date,created_time,edit_time) VALUES
  ('Audi A6',2014,'2.0 TDI Driven: 100 000km', 5, '1ff-33','motorized',150, 40.00,'/home/alija/SEPM/imgs/audia6.jpg' , 'B','2015-11-11','2018-03-30','09:15:00','12:19:05'),
  ('Yamaha',2010,'2.0 TDI Driven: 323 000km', 2, 'dd43-44','motorized',220, 50.00,'/home/alija/SEPM/imgs/yamaha.jpg', 'AB','2015-01-04','2018-03-30','09:15:00','12:19:05'),
  ('Golf 7',2016, null, 5, '1wfb-33','motorized',84, 43,'/home/alija/SEPM/imgs/golf7.jpg','B','2015-05-30','2018-03-30','09:15:00','12:19:05'),
  ('Alfa Giulia',2015,'Driven: 94 442km', 5, '1ff-fgb','motorized',159, 55.00,'/home/alija/SEPM/imgs/giulia.jpg','B','2015-05-12','2018-03-29','09:15:00','12:19:05'),
  ('Segway',2017,'Comfort for traveling through city', 0, null,'motorized',30, 20.00,'/home/alija/SEPM/imgs/segway.jpg','NOLICENSE' ,'2015-12-12','2018-03-30','09:15:00','12:19:05'),
  ('Bike Nakamura',2010,'For best bike driving experience', 1, null,'muscle-powered',null, 25.00,NULL ,'NOLICENSE' ,'2015-01-11','2018-03-30','09:15:00','12:19:05'),
  ('Mercedes Sprinter',2014,'2.0 TDI Driven: 140 443m', 9, '1ffd-fv','motorized',150, 40.00,NULL ,'BC','2015-11-11','2018-03-30','09:15:00','12:19:05'),
  ('Mercedes S Coupe',2017,'300d AMG Driven: 50 000km', 4, '44-ffr4','motorized',300, 250.00,'/home/alija/SEPM/imgs/benzs.jpg','B','2015-02-02','2018-03-30','09:15:00','12:19:05'),
  ('BMW 720xd',2011,'2.0 TDI Driven: 230 000km', 5, '1ff-33','motorized',200, 50.00,'/home/alija/SEPM/imgs/bmw7.jpg' ,'B','2015-11-26','2018-03-30','09:15:00','12:19:05'),
  ('Audi A3',2012,'3.0 TDI Driven: 133 323km', 5, '1ff-32','motorized',120, 20.00,'/home/alija/SEPM/imgs/audia3.jpg' ,'B','2015-10-11','2018-03-30','09:15:00','12:19:05');



INSERT INTO Booking (name, card_number, begin_date, end_date, begin_time, end_time, create_timestamp,bill_timestamp,status,bill_number) VALUES
  ('Maxi Oswald','AT022011100003429660','2017-10-12','2017-10-14','11:00','15:00','2017-09-09 12:39:45','2018-01-01 15:45:00','billed','101'),
  ('Ana Mamic','AT023500000001070671','2017-10-12','2017-11-14','14:00','16:00','2017-09-09 12:25:45',NULL ,'open',NULL ),
  ('Raphael Moser','AT026000000001349870','2017-05-12','2017-06-25','08:00','15:00','2017-12-31 12:39:45',NULL ,'open',NULL ),
  ('Maria Magda','AT021700000432040976','2017-12-06','2017-12-12','11:00','15:00','2017-10-10 12:25:45','2017-12-05 10:10:25 ','canceled','102'),
  ('Chris Lehner','AT022011100003429660','2018-08-25','2018-08-25','10:00','16:00','2018-05-05 12:39:45',NULL ,'open',NULL),
  ('John Lorenz','AT022011100003429660','2018-01-12','2018-01-14','11:00','12:00','2017-12-12 12:39:45','2018-02-05 12:01:05','billed','103'),
  ('Anida Kurtalic','AT023500000001070671','2018-05-12','2018-05-15','14:00','16:00','2018-02-02 12:25:45',NULL ,'open',NULL ),
  ('Mersad Dedic','AT026000000001349870','2018-04-12','2018-04-12','08:00','15:00','2017-12-31 12:25:45',NULL ,'open',NULL ),
  ('Arif Shabani','AT021700000432040976','2017-10-15','2017-10-22','11:00','15:00','2017-09-05 12:39:45','2017-10-15 10:05:02','canceled','104'),
  ('Sebastian Lang','AT022011100003429660','2018-06-25','2018-06-25','07:00','16:00','2018-05-05 12:25:45',NULL ,'open',NULL);


INSERT INTO bookedVehicle (bid, vid, license_number, license_date, price_per_hour, vehicle_name) VALUES
  (1, 5, NULL, NULL, 20.00, 'Segway'),
  (1, 2, 'k4kk32', '2015-05-29', 50.00, 'Yamaha'),
  (2, 7, 'fs32lf3', '2009-10-02', 40.00, 'Mercedes Sprinter'),
  (3, 1, 'n424sm3', '2005-10-09', 40.00, 'Audi A6'),
  (3, 10, 'o9424f22', '2010-09-12', 20.00, 'Audi A3'),
  (4, 3, '2kfs222', '2005-10-25', 50.00, 'Golf 7'),
  (5, 9, '1r2i255', '2008-10-30', 50.00, 'BMW 720xd'),
  (6, 4, '3421f', '2011-05-05', 55.00, 'Alfa Giulia'),
  (7, 6, NULL, NULL, 25.00, 'Bike Nakamura'),
  (8,8,'4234k3','2006-10-12',250.00,'Mercedes S Coupe'),
  (9,3,'424223f','2010-12-12',50.00,'Golf 7'),
  (10,9,'342f22','2011-10-10',50.00,'BMW 720xd');



