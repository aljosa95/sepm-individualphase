import at.ac.tuwien.sepm.assignment.individual.dao.BookingDAO;
import at.ac.tuwien.sepm.assignment.individual.dao.DAOException;
import at.ac.tuwien.sepm.assignment.individual.dao.VehicleDAO;
import at.ac.tuwien.sepm.assignment.individual.dao.impl.BookingDAOJDBC;
import at.ac.tuwien.sepm.assignment.individual.dao.impl.VehicleDAOJDBC;
import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;
import at.ac.tuwien.sepm.assignment.individual.util.DBUtil;
import javafx.collections.ObservableList;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by alija on 08.04.18
 */
public class VehicleDAOTest {

    private Connection connection = null;
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Before
    public void setUp() throws SQLException {
        VehicleDAO vehicleDAO = new VehicleDAOJDBC();
        BookingDAO bookingDAO = new BookingDAOJDBC();
        setBookingDAO(bookingDAO);
        setVehicleDAO(vehicleDAO);

        connection = DBUtil.getConnection();
        assert connection != null;
        connection.setAutoCommit(false);
        LOG.debug("Connection created!");
    }

    @After
    public void tearDown() throws SQLException{
        connection = DBUtil.getConnection();
        assert connection != null;
        connection.rollback();
        LOG.debug("Connection rollback!");
    }


    private VehicleDAO vehicleDAO;

    public void setVehicleDAO(VehicleDAO vehicleDAO) {
        this.vehicleDAO = vehicleDAO;
    }

    private BookingDAO bookingDAO;

    public void setBookingDAO(BookingDAO bookingDAO) {
        this.bookingDAO = bookingDAO;
    }

    /*
      Creating Vehicle Test
     */

    @Test(expected = DAOException.class)
    public void createVehicleWithNullException() throws DAOException{
        vehicleDAO.create(null);
    }

    @Test
    public void createVehicleWithValidInput() throws DAOException {
        Vehicle vehicle = new Vehicle();
        vehicle.setName("Bicycle Test");
        vehicle.setBuildYear(2012);
        vehicle.setPricePerHour(10.0);
        vehicle.setTypeOfDrive("muscle-powered");
        vehicle.setVehicleClass(DriverLicense.NOLICENSE);


        ObservableList<Vehicle> vehicles = vehicleDAO.findAll();
        Assert.assertFalse(vehicles.contains(vehicle));
        vehicleDAO.create(vehicle);
        ObservableList<Vehicle> vehiclesNew = vehicleDAO.findAll();
        for (Vehicle v : vehiclesNew) {
            if (v.getId().equals(vehicle.getId())){
                Assert.assertEquals(v.getBuildYear(),vehicle.getBuildYear());
                Assert.assertEquals(v.getName(),vehicle.getName());
            }
        }
    }

    @Test(expected = DAOException.class)
    public void createWithInvalidYear() throws DAOException{
        Vehicle v = new Vehicle("testing", null, "muscle-powered", 20.0);
        v.setVehicleClass(DriverLicense.NOLICENSE);
        vehicleDAO.create(v);

    }

    @Test
    public void createWithPowerNull() throws DAOException {
        Vehicle vehicle = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        vehicle.setVehicleClass(DriverLicense.NOLICENSE);
        vehicle.setPower(null);
        vehicleDAO.create(vehicle);

        ObservableList<Vehicle> vehicles = vehicleDAO.findAll();
        Assert.assertFalse(vehicles.contains(vehicle));
        vehicleDAO.create(vehicle);
        ObservableList<Vehicle> vehiclesNew = vehicleDAO.findAll();
        for (Vehicle v : vehiclesNew) {
            if (v.getId().equals(vehicle.getId())){
                Assert.assertEquals(v.getBuildYear(),vehicle.getBuildYear());
                Assert.assertEquals(v.getName(),vehicle.getName());
            }
        }
    }

    /*
      Editing Vehicle Test
     */

    @Test(expected = DAOException.class)
    public void updateVehicleWithNull() throws DAOException {
        vehicleDAO.update(null);
    }

    @Test
    public void updateVehicleWithValidData() throws DAOException {
        Vehicle vehicle = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        vehicle.setVehicleClass(DriverLicense.NOLICENSE);
        vehicleDAO.create(vehicle);
        vehicle.setName("bike Edited");
        vehicleDAO.update(vehicle);

        ObservableList<Vehicle> vehiclesNew = vehicleDAO.findAll();
        for (Vehicle v : vehiclesNew) {
            if (v.getId().equals(vehicle.getId())){
                Assert.assertEquals(v.getBuildYear(),vehicle.getBuildYear());
                Assert.assertEquals(v.getName(),vehicle.getName());
            }
        }
    }

    @Test(expected = DAOException.class)
    public void updateVehicleWithInvalidData() throws DAOException {
        Vehicle vehicle = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        vehicle.setVehicleClass(DriverLicense.NOLICENSE);
        vehicleDAO.create(vehicle);
        vehicle.setBuildYear(null);
        vehicleDAO.update(vehicle);
    }

    /*
        Search Vehicles Test
     */

    @Test(expected = DAOException.class)
    public void searchWithNullVehicles() throws DAOException {
        vehicleDAO.searchVehicles(null);
    }

    @Test
    public void emptySearch() throws DAOException {
        Vehicle vehicle = new Vehicle();
        Vehicle vehicle2 = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        vehicle2.setVehicleClass(DriverLicense.NOLICENSE);
        vehicleDAO.create(vehicle2);
        Assert.assertTrue(!vehicleDAO.searchVehicles(vehicle).isEmpty());
    }

    @Test
    public void searchWithValidValues() throws DAOException {
        Vehicle vehicle = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        vehicle.setVehicleClass(DriverLicense.NOLICENSE);
        vehicleDAO.create(vehicle);
        Vehicle search = new Vehicle();
        search.setName("Bike");

        ObservableList<Vehicle> vehicles = vehicleDAO.searchVehicles(search);
        for (Vehicle v : vehicles) {
            if (v.getId().equals(vehicle.getId())){
                Assert.assertEquals(v.getPricePerHour(),vehicle.getPricePerHour());
                Assert.assertEquals(v.getName(),vehicle.getName());
            }
        }
    }

    /*
      Delete Vehicle Test
     */

    @Test (expected = DAOException.class)
    public void deleteWithNull() throws DAOException {
        vehicleDAO.delete(null);
    }

    @Test
    public void deleteWithValidVehicle() throws DAOException {
        Vehicle vehicle = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        vehicle.setVehicleClass(DriverLicense.NOLICENSE);
        vehicleDAO.create(vehicle);
        ObservableList<Vehicle> vehicles = vehicleDAO.findAll();
        for (Vehicle v : vehicles) {
            if (v.getId().equals(vehicle.getId())){
                Assert.assertEquals(v.getBuildYear(),vehicle.getBuildYear());
                Assert.assertEquals(v.getName(),vehicle.getName());
            }
        }
        vehicleDAO.delete(vehicle);
        vehicles = vehicleDAO.findAll();
        Assert.assertFalse(vehicles.contains(vehicle));

    }
}


