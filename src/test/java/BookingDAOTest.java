import at.ac.tuwien.sepm.assignment.individual.dao.BookingDAO;
import at.ac.tuwien.sepm.assignment.individual.dao.DAOException;
import at.ac.tuwien.sepm.assignment.individual.dao.VehicleDAO;
import at.ac.tuwien.sepm.assignment.individual.dao.impl.BookingDAOJDBC;
import at.ac.tuwien.sepm.assignment.individual.dao.impl.VehicleDAOJDBC;
import at.ac.tuwien.sepm.assignment.individual.entities.BookedVehicle;
import at.ac.tuwien.sepm.assignment.individual.entities.Booking;
import at.ac.tuwien.sepm.assignment.individual.entities.Vehicle;
import at.ac.tuwien.sepm.assignment.individual.ui.div.DriverLicense;
import at.ac.tuwien.sepm.assignment.individual.util.DBUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collections;

/**
 * Created by alija on 08.04.18
 */
public class BookingDAOTest  {

    private Connection connection = null;
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Before
    public void setUp() throws SQLException {
        BookingDAO bookingDAO = new BookingDAOJDBC();
        VehicleDAO vehicleDAO = new VehicleDAOJDBC();
        setBookingDAO(bookingDAO);
        setVehicleDAO(vehicleDAO);

        connection = DBUtil.getConnection();
        assert connection != null;
        connection.setAutoCommit(false);
        LOG.debug("Connection created!");
    }

    @After
    public void tearDown() throws SQLException{
        connection = DBUtil.getConnection();
        assert connection != null;
        connection.rollback();
        LOG.debug("Connection rollback!");
    }

    private BookingDAO bookingDAO;
    private VehicleDAO vehicleDAO;

    public void setBookingDAO(BookingDAO bookingDAO) {
        this.bookingDAO = bookingDAO;
    }

    public void setVehicleDAO(VehicleDAO vehicleDAO) {
        this.vehicleDAO = vehicleDAO;
    }

    /*
     Create Booking Test
     */

    @Test(expected = DAOException.class)
    public void createNullBooking() throws DAOException {
        bookingDAO.create(null);
    }

    @Test
    public void createValidEmptyBooking() throws DAOException {
        Booking booking = new Booking("test","DE02120300000000202051", LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        bookingDAO.create(booking);
        ObservableList<Booking> bookings = bookingDAO.findAll();
        for (Booking b: bookings) {
            if (b.getId().equals(booking.getId())){
                Assert.assertEquals(b.getCustomerName(),booking.getCustomerName());
                Assert.assertEquals(b.getCardNumber(),booking.getCardNumber());
            }
        }
    }

    @Test (expected = DAOException.class)
    public void createBookingInvalidInput() throws DAOException {
        Booking booking = new Booking(null,"DE02120300000000202051", LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        bookingDAO.create(booking);
    }

    @Test
    public void updateBookingWithVehicle() throws DAOException {
        Booking booking = new Booking("test","DE02120300000000202051", LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        Vehicle vehicle = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        vehicle.setVehicleClass(DriverLicense.NOLICENSE);

        bookingDAO.create(booking);
        vehicleDAO.create(vehicle);

        BookedVehicle bookedVehicle = new BookedVehicle();
        bookedVehicle.setVid(vehicle.getId());
        bookedVehicle.setBid(booking.getId());
        bookedVehicle.setName(vehicle.getName());
        bookedVehicle.setPricePerHour(vehicle.getPricePerHour());
        booking.setBookedVehicles(FXCollections.observableArrayList(Collections.singletonList(bookedVehicle)));
        bookingDAO.update(booking);

        ObservableList<Booking> bookings = bookingDAO.findAll();
        for (Booking b: bookings) {
            if (b.getId().equals(booking.getId())){
                Assert.assertTrue(!booking.getBookedVehicles().isEmpty());
            }
        }

    }


    @Test (expected = DAOException.class)
    public void createBookingWithUnvalidCardNumber() throws DAOException {
        Booking booking = new Booking("test",null, LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        bookingDAO.create(booking);
    }


    @Test (expected = DAOException.class)
    public void updateBookingWithInvalidData() throws DAOException {
        Booking booking = new Booking("test","DE02120300000000202051", LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        Vehicle vehicle = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        vehicle.setVehicleClass(DriverLicense.NOLICENSE);

        bookingDAO.create(booking);
        vehicleDAO.create(vehicle);

        BookedVehicle bookedVehicle = new BookedVehicle();
        bookedVehicle.setVid(null);
        bookedVehicle.setBid(booking.getId());
        bookedVehicle.setName(vehicle.getName());
        bookedVehicle.setPricePerHour(vehicle.getPricePerHour());
        booking.setBookedVehicles(FXCollections.observableArrayList(Collections.singletonList(bookedVehicle)));
        bookingDAO.update(booking);

    }

    @Test (expected = DAOException.class)
    public void billBookingInvalid() throws DAOException {
        Booking booking = new Booking("test","DE02120300000000202051", LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        bookingDAO.create(booking);
        bookingDAO.billBooking(booking);
    }

    @Test (expected = DAOException.class)
    public void cancelBookingInvalid() throws DAOException {
        Booking booking = new Booking("test","DE02120300000000202051", LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        bookingDAO.cancelBooking(booking);
    }

    @Test
    public void cancelBookingValid() throws DAOException {
        Booking booking = new Booking("test","DE02120300000000202051", LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        bookingDAO.create(booking);
        booking.setBillNumber(150);
        booking.setStatus("canceled");
        bookingDAO.billBooking(booking);
    }

    @Test
    public void billBookingValid() throws DAOException {
        Booking booking = new Booking("test","DE02120300000000202051", LocalDate.parse("2018-03-10"),LocalDate.parse("2018-03-12"),"10:00","15:00");
        bookingDAO.create(booking);
        booking.setStatus("billed");
        booking.setBillNumber(120);
        bookingDAO.billBooking(booking);
    }

    @Test(expected = DAOException.class)
    public void createBookingInvalidDateBegin() throws DAOException {
        Booking booking = new Booking("test","DE02120300000000202051", null,LocalDate.parse("2018-04-12"),"10:00","15:00");
        bookingDAO.create(booking);
    }

    @Test
    public void createBookingWithMoreVehicles() throws DAOException {
        Booking booking = new Booking("testing","DE02120300000000202051", LocalDate.parse("2018-04-10"),LocalDate.parse("2018-04-12"),"10:00","15:00");
        Vehicle vehicle = new Vehicle("Bike Test",2015,"muscle-powered",15.0);
        Vehicle vehicle2 = new Vehicle("segway",2017,"muscle-powered",15.0);

        vehicle.setVehicleClass(DriverLicense.NOLICENSE);
        vehicle2.setVehicleClass(DriverLicense.NOLICENSE);

        bookingDAO.create(booking);
        vehicleDAO.create(vehicle);
        vehicleDAO.create(vehicle2);

        BookedVehicle bookedVehicle = new BookedVehicle();
        bookedVehicle.setVid(vehicle.getId());
        bookedVehicle.setBid(booking.getId());
        bookedVehicle.setName(vehicle.getName());
        bookedVehicle.setPricePerHour(vehicle.getPricePerHour());
        BookedVehicle bookedVehicle2 = new BookedVehicle();
        bookedVehicle2.setVid(vehicle2.getId());
        bookedVehicle2.setBid(booking.getId());
        bookedVehicle2.setName(vehicle2.getName());
        bookedVehicle2.setPricePerHour(vehicle2.getPricePerHour());
        booking.setBookedVehicles(FXCollections.observableArrayList(bookedVehicle,bookedVehicle2));
        bookingDAO.update(booking);

        ObservableList<Booking> bookings = bookingDAO.findAll();
        for (Booking b: bookings) {
            if (b.getId().equals(booking.getId())){
                Assert.assertTrue(booking.getBookedVehicles().size() == 2);
            }
        }
    }
}
